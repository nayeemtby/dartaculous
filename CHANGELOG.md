# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 2023-12-04

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`csharp_generator` - `v0.0.1+2`](#csharp_generator---v0012)

---

#### `csharp_generator` - `v0.0.1+2`

 - **FIX**: updated analyzer dependency to ^6.0.0.


## 2023-12-03

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`squarealfa_entity_generator` - `v3.1.1`](#squarealfa_entity_generator---v311)

---

#### `squarealfa_entity_generator` - `v3.1.1`

 - **FIX**: downgraded analyzer dependency to ^6.0.0.


## 2023-12-03

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`map_mapper_generator` - `v4.2.2`](#map_mapper_generator---v422)
 - [`proto_generator` - `v4.2.2`](#proto_generator---v422)

---

#### `map_mapper_generator` - `v4.2.2`

 - **FIX**: downgraded analyzer dependency to ^6.0.0.

#### `proto_generator` - `v4.2.2`

 - **FIX**: downgraded analyzer dependency to ^6.0.0.


## 2023-12-03

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`proto_generator` - `v4.2.1`](#proto_generator---v421)

---

#### `proto_generator` - `v4.2.1`

 - **FIX**: upgrade squarealfa_generators_common to non-dev version.


## 2023-12-03

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`proto_generator` - `v4.2.0`](#proto_generator---v420)

---

#### `proto_generator` - `v4.2.0`

 - **FEAT**: upgraded dependencies.


## 2023-12-03

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`map_mapper_generator` - `v4.2.1`](#map_mapper_generator---v421)

---

#### `map_mapper_generator` - `v4.2.1`

 - **FIX**: restored analyzer dependency.
 - **FIX**: restored analyzer dependency.


## 2023-12-03

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`map_mapper_generator` - `v4.2.0`](#map_mapper_generator---v420)

---

#### `map_mapper_generator` - `v4.2.0`

 - **FEAT**: upgraded dependencies.


## 2023-11-13

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`proto_generator` - `v4.1.1`](#proto_generator---v411)

---

#### `proto_generator` - `v4.1.1`

 - **FIX**: wrappers with zero value are no longer decoded as null.


## 2023-11-13

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`map_mapper_generator` - `v4.1.1`](#map_mapper_generator---v411)

---

#### `map_mapper_generator` - `v4.1.1`

 - **FIX**: Generated FieldNames classes respects name field of @MapField annotation.


## 2023-11-08

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`sqfsync` - `v0.2.0+1`](#sqfsync---v0201)

---

#### `sqfsync` - `v0.2.0+1`

 - **FIX**: renamed sqf_synchonizer to sqf_synchronizer.


## 2023-10-07

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`map_mapper_annotations` - `v4.1.0`](#map_mapper_annotations---v410)
 - [`map_mapper_generator` - `v4.1.0`](#map_mapper_generator---v410)
 - [`mongo_mapper` - `v0.2.0+2`](#mongo_mapper---v0202)

Packages with dependency updates only:

> Packages listed below depend on other packages in this workspace that have had changes. Their versions have been incremented to bump the minimum dependency versions of the packages they depend upon in this project.

 - `mongo_mapper` - `v0.2.0+2`

---

#### `map_mapper_annotations` - `v4.1.0`

 - **FEAT**: Added nullFallback field in MapField annotation.

#### `map_mapper_generator` - `v4.1.0`

 - **FEAT**: Added nullFallback field in MapField annotation.


## 2023-09-13

### Changes

---

Packages with breaking changes:

 - [`dbsync` - `v2.0.0`](#dbsync---v200)
 - [`sembast_sync` - `v2.0.0`](#sembast_sync---v200)
 - [`sqfsync` - `v0.2.0`](#sqfsync---v020)

Packages with other changes:

 - There are no other changes in this release.

---

#### `dbsync` - `v2.0.0`

 - **BREAKING** **FEAT**: Added abstract entityType getter to SyncTypeHandler.

#### `sembast_sync` - `v2.0.0`

 - **BREAKING** **FEAT**: Added abstract entityType getter to SyncTypeHandler.

#### `sqfsync` - `v0.2.0`

 - **BREAKING** **FEAT**: Added abstract entityType getter to SyncTypeHandler.


## 2023-06-27

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`dbsync` - `v0.1.0+1`](#dbsync---v0101)
 - [`sqfsync` - `v0.1.0+1`](#sqfsync---v0101)
 - [`sembast_sync` - `v0.1.0+1`](#sembast_sync---v0101)

Packages with dependency updates only:

> Packages listed below depend on other packages in this workspace that have had changes. Their versions have been incremented to bump the minimum dependency versions of the packages they depend upon in this project.

 - `sqfsync` - `v0.1.0+1`
 - `sembast_sync` - `v0.1.0+1`

---

#### `dbsync` - `v0.1.0+1`

 - **FIX**: Fixed broken delete synchronization.


## 2023-06-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`proto_generator` - `v4.1.0`](#proto_generator---v410)

---

#### `proto_generator` - `v4.1.0`

 - **FEAT**: Bumped protobuffer to 3.0.0.


## 2023-06-17

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`cloud_run_grpc_auth` - `v0.11.1+2`](#cloud_run_grpc_auth---v01112)
 - [`csharp_annotations` - `v0.0.1+1`](#csharp_annotations---v0011)
 - [`csharp_generator` - `v0.0.1+1`](#csharp_generator---v0011)
 - [`map_mapper_annotations` - `v4.0.1`](#map_mapper_annotations---v401)
 - [`map_mapper_generator` - `v4.0.1`](#map_mapper_generator---v401)
 - [`mongo_mapper` - `v0.2.0+1`](#mongo_mapper---v0201)
 - [`proto_annotations` - `v3.0.1`](#proto_annotations---v301)
 - [`proto_generator` - `v4.0.1`](#proto_generator---v401)
 - [`squarealfa_entity_annotations` - `v4.0.1`](#squarealfa_entity_annotations---v401)
 - [`squarealfa_entity_generator` - `v3.0.3`](#squarealfa_entity_generator---v303)
 - [`squarealfa_security` - `v3.0.1`](#squarealfa_security---v301)

---

#### `cloud_run_grpc_auth` - `v0.11.1+2`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `csharp_annotations` - `v0.0.1+1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `csharp_generator` - `v0.0.1+1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `map_mapper_annotations` - `v4.0.1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `map_mapper_generator` - `v4.0.1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `mongo_mapper` - `v0.2.0+1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `proto_annotations` - `v3.0.1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `proto_generator` - `v4.0.1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `squarealfa_entity_annotations` - `v4.0.1`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `squarealfa_entity_generator` - `v3.0.3`

 - **FIX**: Upgraded max Dart version to <4.0.0.

#### `squarealfa_security` - `v3.0.1`

 - **FIX**: Upgraded max Dart version to <4.0.0.


## 2023-06-17

### Changes

---

Packages with breaking changes:

 - [`map_mapper_annotations` - `v4.0.0`](#map_mapper_annotations---v400)
 - [`map_mapper_generator` - `v4.0.0`](#map_mapper_generator---v400)
 - [`proto_annotations` - `v3.0.0`](#proto_annotations---v300)
 - [`proto_generator` - `v4.0.0`](#proto_generator---v400)
 - [`squarealfa_security` - `v3.0.0`](#squarealfa_security---v300)
 - [`squarealfa_entity_annotations` - `v4.0.0`](#squarealfa_entity_annotations---v400)

Packages with other changes:

 - [`csharp_annotations` - `v0.0.1`](#csharp_annotations---v001)
 - [`csharp_generator` - `v0.0.1`](#csharp_generator---v001)
 - [`dbsync` - `v0.1.0`](#dbsync---v010)
 - [`mongo_mapper` - `v0.2.0`](#mongo_mapper---v020)
 - [`sembast_sync` - `v0.1.0`](#sembast_sync---v010)
 - [`sqfsync` - `v0.1.0`](#sqfsync---v010)
 - [`squarealfa_entity_generator` - `v3.0.2`](#squarealfa_entity_generator---v302)

Packages graduated to a stable release (see pre-releases prior to the stable version for changelog entries):

 - `csharp_annotations` - `v0.0.1`
 - `csharp_generator` - `v0.0.1`
 - `dbsync` - `v0.1.0`
 - `map_mapper_annotations` - `v4.0.0`
 - `map_mapper_generator` - `v4.0.0`
 - `mongo_mapper` - `v0.2.0`
 - `proto_annotations` - `v3.0.0`
 - `proto_generator` - `v4.0.0`
 - `sembast_sync` - `v0.1.0`
 - `sqfsync` - `v0.1.0`
 - `squarealfa_security` - `v3.0.0`

Packages with dependency updates only:

> Packages listed below depend on other packages in this workspace that have had changes. Their versions have been incremented to bump the minimum dependency versions of the packages they depend upon in this project.

 - `squarealfa_entity_generator` - `v3.0.2`

---

#### `map_mapper_annotations` - `v4.0.0`

#### `map_mapper_generator` - `v4.0.0`

#### `proto_annotations` - `v3.0.0`

#### `proto_generator` - `v4.0.0`

#### `squarealfa_security` - `v3.0.0`

#### `squarealfa_entity_annotations` - `v4.0.0`

 - **FIX**: Updated pubspec with new git URL.
 - **FIX**: Changed repository URL.
 - **FEAT**: Added defaults provider.
 - **FEAT**: removed dependecy from squarealfa_common_types.
 - **BREAKING** **FEAT**: Huge upgrade to proto_generator.

#### `csharp_annotations` - `v0.0.1`

#### `csharp_generator` - `v0.0.1`

#### `dbsync` - `v0.1.0`

#### `mongo_mapper` - `v0.2.0`

#### `sembast_sync` - `v0.1.0`

#### `sqfsync` - `v0.1.0`

