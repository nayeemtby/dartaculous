## 0.2.0+1

 - **FIX**: renamed sqf_synchonizer to sqf_synchronizer.

## 0.2.0

> Note: This release has breaking changes.

 - **BREAKING** **FEAT**: Added abstract entityType getter to SyncTypeHandler.

## 0.1.0+1

 - Update a dependency to the latest release.

## 0.1.0

 - Graduate package to a stable release. See pre-releases prior to this version for changelog entries.

## 0.1.0-dev.1

- Better lifecycle

## 0.0.1-dev.3

- Added support for batch processing.

## 0.0.1-dev.2

- Updated dependency.

## 0.0.1-dev.1

- Initial version.
