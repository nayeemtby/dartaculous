import 'package:drift/drift.dart';
import 'package:drift_sync/drift_sync.dart';

class LocalChange  {
  final int id;
  final String entityType;

  final String entityId;
  final String entityRev;
  final ChangeOperation operation;
  final Uint8List protoBytes;

  final DateTime createMoment;
  final bool concluded;
  final DateTime? concludedMoment;
  final String? error;
  final bool dismissed;

  LocalChange({
    required this.id,
    required this.createMoment,
    required this.entityType,
    required this.entityId,
    required this.entityRev,
    required this.protoBytes,
    required this.operation,
    this.concluded = false,
    this.concludedMoment,
    this.error,
    this.dismissed = false,
  });

  factory LocalChange.create({
    required String entityType,
    required Uint8List protoBytes,
    required String entityId,
    required String entityRev,
  }) {
    return LocalChange(
      createMoment: DateTime.now(),
      operation: ChangeOperation.create,
      entityType: entityType,
      protoBytes: protoBytes,
      entityId: entityId,
      entityRev: entityRev,
      id: -1,
      concluded: false,
      concludedMoment: null,
      error: null,
      dismissed: false,
    );
  }

  factory LocalChange.update({
    required String entityType,
    required Uint8List protoBytes,
    required String entityId,
    required String entityRev,
  }) {
    return LocalChange(
      createMoment: DateTime.now(),
      operation: ChangeOperation.update,
      entityType: entityType,
      protoBytes: protoBytes,
      entityId: entityId,
      entityRev: entityRev,
      id: -1,
      concluded: false,
      concludedMoment: null,
      error: null,
      dismissed: false,
    );
  }

  factory LocalChange.delete({
    required String entityType,
    required Uint8List protoBytes,
    required String entityId,
    required String entityRev,
  }) {
    return LocalChange(
      createMoment: DateTime.now(),
      operation: ChangeOperation.delete,
      entityType: entityType,
      protoBytes: protoBytes,
      entityId: entityId,
      entityRev: entityRev,
      id: -1,
      concluded: false,
      concludedMoment: null,
      error: null,
      dismissed: false,
    );
  }

  LocalChange copyWith({
    int? id,
    String? entityType,
    String? entityId,
    String? entityRev,
    ChangeOperation? operation,
    Uint8List? protoBytes,
    DateTime? createMoment,
    bool? concluded,
    DateTime? concludedMoment,
    String? error,
    bool? dismissed,
  }) {
    return LocalChange(
      id: id ?? this.id,
      entityType: entityType ?? this.entityType,
      entityId: entityId ?? this.entityId,
      entityRev: entityRev ?? this.entityRev,
      operation: operation ?? this.operation,
      protoBytes: protoBytes ?? this.protoBytes,
      createMoment: createMoment ?? this.createMoment,
      concluded: concluded ?? this.concluded,
      concludedMoment: concludedMoment ?? this.concludedMoment,
      error: error ?? this.error,
      dismissed: dismissed ?? this.dismissed,
    );
  }

}


