import 'package:drift_sync/drift_sync.dart';
import 'package:meta/meta.dart';

enum DataSource {
  remote,
  local,
}

enum DataDestination { local, both }

/// Provides methods to get, create, update and delete
/// entities of type <TEntity>.
///
/// Each of these
/// methods works by attempting to first use
/// online data with the fallback of the offline data.
abstract class SyncEntityRepository<TAppDatabase extends SynchronizerDb,
    TEntity, TKey> {
  const SyncEntityRepository({
    required this.syncHandler,
    required this.db,
  });

  final SyncTypeHandler<TEntity, TKey> syncHandler;
  final TAppDatabase db;

  Future<(TEntity, DataSource)> get(TKey id) async {
    final remote = await getRemote(id);
    if (remote != null) {
      await syncHandler.upsertLocal(remote);
      return (remote, DataSource.remote);
    }
    final local = await syncHandler.getLocal(id);
    return (local, DataSource.local);
  }

  @protected
  Future<TEntity?> getRemote(TKey id) async {
    try {
      final e = await syncHandler.getRemote(id);
      return e;
    } on UnavailableException catch (_) {
      return null;
    }
  }

  Future<(TEntity, DataDestination)> create(TEntity entity) async {
    final remoteCreated = await createRemote(entity);
    final created = remoteCreated ?? entity;
    final ds =
        remoteCreated != null ? DataDestination.both : DataDestination.local;
    await this.db.transaction(() async {
      await syncHandler.upsertLocal(entity);
      if (remoteCreated == null) {
        final localChange = LocalChange.create(
          protoBytes: syncHandler.marshal(entity),
          entityType: syncHandler.entityType,
          entityId: syncHandler.getId(entity),
          entityRev: syncHandler.getRev(entity),
        );
        await db.insertLocalChange(localChange);
      } else {
        await db.concludeEntityLocalChanges(
          syncHandler.entityType,
          syncHandler.getId(entity),
          syncHandler.getRev(entity),
        );
      }
    });

    return (created, ds);
  }

  @protected
  Future<TEntity?> createRemote(TEntity entity) async {
    try {
      final created = await syncHandler.createRemote(entity);
      return created;
    } on UnavailableException catch (_) {
      return null;
    }
  }

  Future<(TEntity, DataDestination)> update(TEntity entity) async {
    TEntity? remoteUpdated = await updateRemote(entity);
    final updated = remoteUpdated ?? entity;
    final ds =
        remoteUpdated == null ? DataDestination.local : DataDestination.both;

    await db.transaction(() async {
      await syncHandler.upsertLocal(updated);
      if (remoteUpdated == null) {
        final localChange = LocalChange.update(
          entityType: syncHandler.entityType,
          protoBytes: syncHandler.marshal(entity),
          entityId: syncHandler.getId(entity),
          entityRev: syncHandler.getRev(entity),
        );
        await db.insertLocalChange(localChange);
      } else {
        await db.concludeEntityLocalChanges(
          syncHandler.entityType,
          syncHandler.getId(entity),
          syncHandler.getRev(entity),
        );
      }
    });

    return (updated, ds);
  }

  /// Tries to update remotely.
  /// Returns:
  /// - The updated entity if succeed
  /// - null if unavailable
  /// throws if any other exception
  @protected
  Future<TEntity?> updateRemote(TEntity entity) async {
    try {
      final updated = await syncHandler.updateRemote(entity);
      return updated;
    } on UnavailableException catch (_) {
      return null;
    }
  }

  Future<DataDestination> delete(TEntity entity) async {
    final synced = await deleteRemote(entity);
    final ds = synced ? DataDestination.both : DataDestination.local;

    await db.transaction(() async {
      await syncHandler.deleteLocal(entity);
      if (!synced) {
        final localChange = LocalChange.delete(
          entityType: syncHandler.entityType,
          protoBytes: syncHandler.marshal(entity),
          entityId: syncHandler.getId(entity),
          entityRev: syncHandler.getRev(entity),
        );
        await db.insertLocalChange(localChange);
      } else {
        await db.concludeEntityLocalChanges(
          syncHandler.entityType,
          syncHandler.getId(entity),
          syncHandler.getRev(entity),
        );
      }
    });

    return ds;
  }

  // Tries to delete the entity remotely
  // Returns:
  // - true if succeeded
  // - false if unavailable
  // throws if any other exception
  Future<bool> deleteRemote(TEntity entity) async {
    try {
      await syncHandler.deleteRemote(entity);
      return true;
    } on UnavailableException catch (_) {
      return false;
    }
  }
}
