import 'dart:async';

import 'package:drift_sync/drift_sync.dart';
import 'package:logging/logging.dart';
import 'package:meta/meta.dart';

final _logger = Logger('dbsync:Synchronizer');

abstract class DriftSynchronizer<TAppDatabase extends SynchronizerDb> {
  DriftSynchronizer({
    required this.appDatabase,
    required this.typeHandlers,
  }) : _typeHandlers = <String, SyncTypeHandler>{
          for (final th in typeHandlers) th.entityType: th
        };

  SyncState _state = const SyncState.initial();
  SyncState get state => _state;

  final Set<SyncTypeHandler> typeHandlers;
  final Map<String, SyncTypeHandler> _typeHandlers;
  final TAppDatabase appDatabase;

  /// Gets the Id of the latest available change from the server.
  @protected
  Future<String?> getLatestServerChangeId();

  /// Gets the pending changes from the server, starting
  /// with lastChangeId.
  /// Will return an empty stream if no changes are available,
  /// but lastChangeId still exists in the change log.
  /// Will return null if lastChangeId has expired and removed from the
  /// change log.
  @protected
  Future<List<ServerChange>> getServerPendingChanges(String? lastChangeId);

  @protected
  Future<void> Function()? get onStarted => null;

  @protected
  Future<void> Function(SyncState state)? get onStopped => null;

  @protected
  Future<void> Function()? get onCancelRequested => null;

  @protected
  Future<void> Function(SyncState previous, SyncState current)?
      get onStateChanged => null;

  Future<void> _updateState(SyncState state) async {
    final previous = this._state;
    if (previous == state) {
      return;
    }
    this._state = state;
    if (!previous.isSynchronizing && state.isSynchronizing) {
      this.onStarted?.call();
    }
    if (!previous.cancelRequested && state.cancelRequested) {
      this.onCancelRequested?.call();
    }
    if (previous.isSynchronizing && !state.isSynchronizing) {
      this.onStopped?.call(state);
    }

    this.onStateChanged?.call(previous, state);
  }


  /// Synchronizes pending local changes to the server and tries
  /// to do sync the pending changes from the server to the app.
  /// When it is not possible to do a consistent synchronization
  /// of the pending  changes from the server, reverts to a
  /// full synchronization from the server.
  Future<void> sync() async {
    _preventConcurrentSync();
    _updateState(state.start());
    try {
      await syncLocalChanges();
      await syncServerChanges();
    } finally {
      _updateState(state.stop());
    }
  }

  /// Synchronizes pending local changes to the server does
  /// full synchronization from the server.
  Future<void> fullResync() async {
    _preventConcurrentSync();
    _updateState(state.start());
    try {
      await syncLocalChanges();
      await _fullResync();
    } finally {
      _updateState(state.stop());
    }
  }

  void cancel() {
    _updateState(state.cancel());
  }

  void _preventConcurrentSync() {
    if (state.isSynchronizing) {
      throw const InvalidStateException(
          message: "there is another synchronization already running");
    }
  }

  /**********************************
   *      Upload Synchronization    *
   **********************************/

  /// synchronizes all local changes to the server
  /// Returns a list of local changes that were discarded
  /// because of optimistic conflict
  Future<void> syncLocalChanges() async {
    final localChanges = await appDatabase.getPendingLocalChanges();

    for (final localChange in localChanges) {
      if (_state.cancelRequested) {
        break;
      }
      final handler = _getTypeHandlerByTypeName(localChange.entityType);

      try {
        await this.appDatabase.transaction(() async {
          await appDatabase.concludeLocalChange(localChange);
          await _doOperation(localChange, handler);
        });
      } on ConflictException catch (ex) {
        await appDatabase.concludeLocalChange(localChange,
            error: ex.toString());
        this._updateState(state.withConflict());
      } catch (ex) {
        await appDatabase.logLocalChangeError(localChange, ex.toString());
        rethrow;
      }
    }
  }

  Future<void> _doOperation(LocalChange localChange,
      SyncTypeHandler<dynamic, dynamic> handler) async {
    switch (localChange.operation) {
      case ChangeOperation.create:
        final entity = handler.unmarshal(localChange.protoBytes);
        final updated = await handler.createRemote(entity);
        await handler.upsertLocal(updated);
        break;
      case ChangeOperation.update:
        final entity = handler.unmarshal(localChange.protoBytes);
        final updated = await handler.updateRemote(entity);
        await handler.upsertLocal(updated);
        break;
      case ChangeOperation.delete:
        final entity = handler.unmarshal(localChange.protoBytes);
        await handler.deleteRemote(entity);
        break;
    }
  }

  SyncTypeHandler _getTypeHandlerByTypeName(String typeName) {
    final handler = _typeHandlers[typeName];
    if (handler == null) {
      throw ArgumentError(
          "There is no handler registered for the entity's type", 'entity');
    }
    return handler;
  }

  /**********************************
   *      Download Synchronization    *
   **********************************/

  /// tries to do a partial synchronization from the server,
  /// but falls back to full synchronization when needed
  Future<void> syncServerChanges() async {
    _logger.finest('Entered DownloadSynchronizer.sync method');
    final lastChangeId = await this.appDatabase.getLastChangeId();

    if (lastChangeId == null) {
      _logger.finest('... no lastChangeId, so will do full resync');
      await _fullResync();
      return;
    }
    _logger.finest('... will sync from $lastChangeId');
    try {
      final changes = await getServerPendingChanges(
          lastChangeId == '' ? null : lastChangeId);
      await _partialSyncServerChanges(changes);
    } on NotFoundException catch (_) {
      _logger.finest('...Received a NotFoundException, so doing a fullResync');
      await _fullResync();
    }
  }

  Future<void> _partialSyncServerChanges(List<ServerChange> changes) async {
    _logger.finest('Entered _partialSyncServerChanges');
    try {
      await appDatabase.transaction(() async {
        ServerChange? lastChange;

        int cnt = 0;
        for (final change in changes) {
          if (_state.cancelRequested) {
            _logger.finest('... cancel requested. Leaving');
            throw const CancelException();
          }
          if ((++cnt % 10000) == 0) {
            _logger.finest('synching ${cnt}th item of ${change.entityType}');
          }
          final handler = _getTypeHandlerByTypeName(change.entityType);

          switch (change.changeOperation) {
            case ChangeOperation.delete:
              final entity = await handler.unmarshal(change.entity);
              await handler.deleteLocal(entity);
              break;
            case ChangeOperation.create:
            case ChangeOperation.update:
              final entity = await handler.unmarshal(change.entity);
              await handler.upsertLocal(entity);
              break;
            default:
              throw UnsupportedError('Type of change not supported.');
          }
          lastChange = change;
        }
        _logger.finest('... received all changes.');
        if (lastChange != null) {
          _logger
              .finest('... will setLastReceivedChangeId to ${lastChange.id}');

          final lcId = lastChange.id;
          await appDatabase.setLastReceivedChangeId(lcId);
        }
      });
    } on CancelException catch (_) {
      _logger.finest('user cancelled sync');
      rethrow;
    } catch (ex) {
      _logger.finest('exception on _partialSyncServerChanges: $ex');
      rethrow;
    }
    _logger.finest('finished _partialSyncServerChanges with no incident');
  }

  Future<void> _fullResync() async {
    final sw = Stopwatch();
    sw.start();
    _logger.finest('Entered fullResync');
    try {
      await appDatabase.transaction(() async {
        _logger.finest('... will call getLatestServerChangeId');
        final lastSyncedChangeId = await getLatestServerChangeId();
        _logger.finest(
            '... got $lastSyncedChangeId. Will clear all local changes');
        await appDatabase.cancelAllLocalChanges();
        _logger.finest('... will reset last received changedId to null');
        await appDatabase.setLastReceivedChangeId(null);
        _logger.finest('... will clear all local records');
        for (final handler in _typeHandlers.values) {
          _logger.finest(
              '... will clear local records for handler ${handler.toString()}');
          await handler.deleteAllLocal();
        }
        _logger.finest('... will sync each handler');

        for (final handler in _typeHandlers.values) {
          _logger.finest(
              '... syncing all items for handler ${handler.toString()}');
          if (_state.cancelRequested) {
            _logger.finest('... cancel requested. Will leave.');
            throw const CancelException();
          }
          _logger.info('started handler for ${handler.entityType}');

          final list = await handler.getAllRemote();
          _logger.info('got all ${sw.elapsedMilliseconds}');

          await handler.upsertAllLocal(list);
          _logger.info('called insertAll ${sw.elapsedMilliseconds}');

          _logger
              .info('synced ${handler.toString()} ${sw.elapsedMilliseconds}');
          _logger.finest('... done syncing all items for handler.');
        }
        _logger.finest(
            '... done syncing all handlers will call setLastReceivedChangeId with $lastSyncedChangeId.');
        await appDatabase.setLastReceivedChangeId(lastSyncedChangeId ?? '');
        sw.stop();
        _logger.info(
            'synchronization terminated after taking ${sw.elapsedMilliseconds} milliseconds');
      });
    } on CancelException catch (_) {
      _logger.finest('user cancelled sync');
      rethrow;
    }
  }
}
