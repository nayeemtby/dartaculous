import 'package:drift/drift.dart';
import 'package:drift_sync/drift_sync.dart';

mixin SynchronizerDb on GeneratedDatabase {
  Future<List<LocalChange>> getPendingLocalChanges();
  Future<void> cancelAllLocalChanges();
  Future<void> concludeLocalChange(LocalChange localChange, {String? error});
  Future<void> logLocalChangeError(LocalChange localChange, String error);
  Future<String?> getLastChangeId();
  Future<void> setLastReceivedChangeId(String? id);
  Future<void> insertLocalChange(LocalChange localChange);
  Future<void> concludeEntityLocalChanges(
    String entityType,
    String entityId,
    String entityRev,
  );
}
