import 'package:drift/drift.dart';
import 'package:drift_sync/drift_sync.dart';

class LocalChanges extends Table {
  IntColumn get id => integer().autoIncrement()();
  DateTimeColumn get createMoment => dateTime()();
  TextColumn get entityType => text()();
  TextColumn get entityId => text()();
  TextColumn get entityRev => text()();
  IntColumn get operation => integer().map(const ChangeOperationConverter())();
  BlobColumn get protoBytes => blob()();

  BoolColumn get concluded => boolean().withDefault(const Constant(true))();
  DateTimeColumn get concludedMoment => dateTime().nullable()();
  TextColumn get error => text().nullable()();
  BoolColumn get dismissed => boolean()();
}

class LocalChangeSyncLog extends Table {
  IntColumn get localChangeId => integer().references(
        LocalChanges,
        #id,
        onDelete: KeyAction.cascade,
      )();
  DateTimeColumn get moment => dateTime()();
  TextColumn get error => text().nullable()();
}

class ChangeOperationConverter extends TypeConverter<ChangeOperation, int> {
  const ChangeOperationConverter();

  @override
  ChangeOperation fromSql(int fromDb) {
    return ChangeOperation.values[fromDb];
  }

  @override
  int toSql(ChangeOperation value) {
    return ChangeOperation.values.indexOf(value);
  }
}
