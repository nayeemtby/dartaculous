
import 'package:drift_sync/drift_sync.dart';
import 'package:grpc/grpc.dart';


mixin DownloadSyncTypeHandler<TEntity, TKey> on SyncTypeHandler<TEntity, TKey> {
  Future<List<TEntity>> grpcGetAllRemote();

  @override
  Future<TEntity?> getRemote(TKey id) async {
    return null;
  }

  @override
  Future<List<TEntity>> getAllRemote() async {
    try {
      final str = await grpcGetAllRemote();
      return str;
    } on GrpcError catch (exception) {
      if (exception.code == StatusCode.unavailable) {
        throw UnavailableException(innerException: exception);
      }
      rethrow;
    }
  }

  @override
  Future<TEntity> createRemote(TEntity entity) async {
    throw UnimplementedError();
  }

  @override
  Future<TEntity> updateRemote(TEntity entity) async {
    throw UnimplementedError();
  }

  @override
  Future<void> deleteRemote(TEntity entity) async {
    throw UnimplementedError();
  }
}
