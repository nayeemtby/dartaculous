import 'package:drift_sync/drift_sync.dart';
import 'package:grpc/grpc.dart';
import 'package:meta/meta.dart';

mixin GrpcSyncTypeHandler<TEntity, TKey> on SyncTypeHandler<TEntity, TKey> {
  Future<List<TEntity>> grpcGetAllRemote();
  Future<TEntity?> grpcGetRemote(TKey id);
  Future<TEntity> grpcCreateRemote(TEntity entity);
  Future<TEntity> grpcUpdateRemote(TEntity entity);
  Future<void> grpcDeleteRemote(TEntity entity);

  @override
  Future<TEntity?> getRemote(TKey id) async {
    try {
      final e = await grpcGetRemote(id);
      return e;
    } on GrpcError catch (ex) {
      if (isUnavailable(ex)) {
        throw UnavailableException(innerException: ex);
      }
      if (isNotFound(ex)) {
        throw NotFoundException(innerException: ex);
      }
      rethrow;
    }
  }

  @override
  Future<List<TEntity>> getAllRemote() async {
    try {
      final str = await grpcGetAllRemote();
      return str;
    } on GrpcError catch (exception) {
      if (isUnavailable(exception)) {
        throw UnavailableException(innerException: exception);
      }
      if (isNotFound(exception)) {
        throw NotFoundException(innerException: exception);
      }
      rethrow;
    }
  }

  @override
  Future<TEntity> createRemote(TEntity entity) async {
    try {
      final created = await grpcCreateRemote(entity);
      return created;
    } on GrpcError catch (exception) {
      if (isUnavailable(exception)) {
        throw UnavailableException(innerException: exception);
      }
      if (exception.code == StatusCode.aborted ||
          exception.code == StatusCode.alreadyExists) {
        throw ConflictException(innerException: exception);
      }
      rethrow;
    }
  }

  @override
  Future<TEntity> updateRemote(TEntity entity) async {
    try {
      final updated = await grpcUpdateRemote(entity);
      return updated;
    } on GrpcError catch (exception) {
      if (isUnavailable(exception)) {
        throw UnavailableException(innerException: exception);
      }
      if (isNotFound(exception)) {
        throw NotFoundException(innerException: exception);
      }

      if (exception.code == StatusCode.aborted ||
          exception.code == StatusCode.alreadyExists ||
          exception.code == StatusCode.notFound) {
        throw ConflictException(innerException: exception);
      }
      rethrow;
    }
  }

  @override
  Future<void> deleteRemote(TEntity entity) async {
    try {
      await grpcDeleteRemote(entity);
    } on GrpcError catch (exception) {
      if (isUnavailable(exception)) {
        throw UnavailableException(innerException: exception);
      }
      if (isNotFound(exception)) {
        throw NotFoundException(innerException: exception);
      }
      if (exception.code == StatusCode.aborted ||
          exception.code == StatusCode.notFound) {
        throw ConflictException(innerException: exception);
      }
      rethrow;
    }
  }

  @protected
  bool isUnavailable(GrpcError exception) {
    return exception.code == StatusCode.unavailable;
  }

  @protected
  bool isNotFound(GrpcError exception) {
    return exception.code == StatusCode.notFound;
  }
}
