import 'dart:ffi';
import 'dart:io';

import 'package:entra_auth_desktop/src/gen/ffi.pb.dart';
import 'package:go_bridge/gen/google/protobuf/empty.pbserver.dart';
import 'types.dart';
import 'package:go_bridge/gen/common/primitive_messages.pb.dart';
import 'package:go_bridge/helpers.dart';

import 'auth_bindings_generated.dart';

const String _libName = 'adder';

final DynamicLibrary _dylib = () {
  if (Platform.isMacOS || Platform.isIOS) {
    return DynamicLibrary.open('$_libName.framework/$_libName');
  }
  if (Platform.isAndroid || Platform.isLinux) {
    return DynamicLibrary.open('$_libName.so');
  }
  if (Platform.isWindows) {
    return DynamicLibrary.open('$_libName.dll');
  }
  throw UnsupportedError('Unknown platform: ${Platform.operatingSystem}');
}();

final AuthBindings _authBindings = AuthBindings(_dylib);

void _initialize() {
  _authBindings.initialize(NativeApi.initializeApiDLData);
}

Future<Guid> newPublicClient(NewPublicClientRequest request) async {
  _initialize();
  final response = await callGoFunc(
    request: request,
    goFunc: _authBindings.newPublicClient,
    responseToFill: ByteArrayMessage(),
    errorsToThrow: [FfiError()],
  );

  return response.value;
}

Future<void> disposePublicClient(DisposePublicClientRequest request) async {
  await callGoFunc(
    request: request,
    goFunc: _authBindings.disposePublicClient,
    responseToFill: Empty(),
    errorsToThrow: [FfiError()],
  );
}

Future<void> goSignout(SignoutRequest request) async {
  await callGoFunc(
    request: request,
    goFunc: _authBindings.signout,
    responseToFill: Empty(),
    errorsToThrow: [FfiError()],
  );
}

Future<TokenResponse> goGetCachedAccessToken(GetTokenRequest request) async {
  final r = await callGoFunc(
    request: request,
    goFunc: _authBindings.getCachedAccessToken,
    responseToFill: TokenResponse(),
    errorsToThrow: [FfiError()],
  );
  return r;
}

Future<TokenResponse> goAcquireTokenInteractive(GetTokenRequest request) async {
  final r = await callGoFunc(
    request: request,
    goFunc: _authBindings.acquireTokenInteractive,
    responseToFill: TokenResponse(),
    errorsToThrow: [FfiError()],
  );

  return r;
}
