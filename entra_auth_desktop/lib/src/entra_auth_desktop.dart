import 'gen/ffi.pb.dart';
import 'types.dart';

Future<Guid> newPublicClient(NewPublicClientRequest request) {
  throw UnimplementedError();
}

Future<void> disposePublicClient(DisposePublicClientRequest request) async {
  throw UnimplementedError();
}

Future<TokenResponse> goGetCachedAccessToken(GetTokenRequest request) async {
  throw UnimplementedError();
}

Future<void> goSignout(SignoutRequest request) async {
  throw UnimplementedError();
}

Future<TokenResponse> goAcquireTokenInteractive(GetTokenRequest request) async {
  throw UnimplementedError();
}
