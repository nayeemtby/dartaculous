//
//  Generated code. Do not modify.
//  source: ffi.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class ErrorType extends $pb.ProtobufEnum {
  static const ErrorType unspecified = ErrorType._(0, _omitEnumNames ? '' : 'unspecified');
  static const ErrorType id_not_found = ErrorType._(1, _omitEnumNames ? '' : 'id_not_found');

  static const $core.List<ErrorType> values = <ErrorType> [
    unspecified,
    id_not_found,
  ];

  static final $core.Map<$core.int, ErrorType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ErrorType? valueOf($core.int value) => _byValue[value];

  const ErrorType._($core.int v, $core.String n) : super(v, n);
}


const _omitEnumNames = $core.bool.fromEnvironment('protobuf.omit_enum_names');
