//
//  Generated code. Do not modify.
//  source: ffi.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use errorTypeDescriptor instead')
const ErrorType$json = {
  '1': 'ErrorType',
  '2': [
    {'1': 'unspecified', '2': 0},
    {'1': 'id_not_found', '2': 1},
  ],
};

/// Descriptor for `ErrorType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List errorTypeDescriptor = $convert.base64Decode(
    'CglFcnJvclR5cGUSDwoLdW5zcGVjaWZpZWQQABIQCgxpZF9ub3RfZm91bmQQAQ==');

@$core.Deprecated('Use ffiErrorDescriptor instead')
const FfiError$json = {
  '1': 'FfiError',
  '2': [
    {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    {'1': 'errorType', '3': 2, '4': 1, '5': 14, '6': '.ErrorType', '10': 'errorType'},
  ],
};

/// Descriptor for `FfiError`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List ffiErrorDescriptor = $convert.base64Decode(
    'CghGZmlFcnJvchIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEigKCWVycm9yVHlwZRgCIAEoDj'
    'IKLkVycm9yVHlwZVIJZXJyb3JUeXBl');

@$core.Deprecated('Use newPublicClientRequestDescriptor instead')
const NewPublicClientRequest$json = {
  '1': 'NewPublicClientRequest',
  '2': [
    {'1': 'clientId', '3': 1, '4': 1, '5': 9, '10': 'clientId'},
    {'1': 'tenantId', '3': 2, '4': 1, '5': 9, '10': 'tenantId'},
  ],
};

/// Descriptor for `NewPublicClientRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newPublicClientRequestDescriptor = $convert.base64Decode(
    'ChZOZXdQdWJsaWNDbGllbnRSZXF1ZXN0EhoKCGNsaWVudElkGAEgASgJUghjbGllbnRJZBIaCg'
    'h0ZW5hbnRJZBgCIAEoCVIIdGVuYW50SWQ=');

@$core.Deprecated('Use signoutRequestDescriptor instead')
const SignoutRequest$json = {
  '1': 'SignoutRequest',
  '2': [
    {'1': 'clientId', '3': 1, '4': 1, '5': 12, '10': 'clientId'},
  ],
};

/// Descriptor for `SignoutRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List signoutRequestDescriptor = $convert.base64Decode(
    'Cg5TaWdub3V0UmVxdWVzdBIaCghjbGllbnRJZBgBIAEoDFIIY2xpZW50SWQ=');

@$core.Deprecated('Use disposePublicClientRequestDescriptor instead')
const DisposePublicClientRequest$json = {
  '1': 'DisposePublicClientRequest',
  '2': [
    {'1': 'clientId', '3': 1, '4': 1, '5': 12, '10': 'clientId'},
  ],
};

/// Descriptor for `DisposePublicClientRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List disposePublicClientRequestDescriptor = $convert.base64Decode(
    'ChpEaXNwb3NlUHVibGljQ2xpZW50UmVxdWVzdBIaCghjbGllbnRJZBgBIAEoDFIIY2xpZW50SW'
    'Q=');

@$core.Deprecated('Use getTokenRequestDescriptor instead')
const GetTokenRequest$json = {
  '1': 'GetTokenRequest',
  '2': [
    {'1': 'clientId', '3': 1, '4': 1, '5': 12, '10': 'clientId'},
    {'1': 'scopes', '3': 2, '4': 3, '5': 9, '10': 'scopes'},
  ],
};

/// Descriptor for `GetTokenRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getTokenRequestDescriptor = $convert.base64Decode(
    'Cg9HZXRUb2tlblJlcXVlc3QSGgoIY2xpZW50SWQYASABKAxSCGNsaWVudElkEhYKBnNjb3Blcx'
    'gCIAMoCVIGc2NvcGVz');

@$core.Deprecated('Use tokenResponseDescriptor instead')
const TokenResponse$json = {
  '1': 'TokenResponse',
  '2': [
    {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `TokenResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tokenResponseDescriptor = $convert.base64Decode(
    'Cg1Ub2tlblJlc3BvbnNlEhQKBXRva2VuGAEgASgJUgV0b2tlbg==');

