import 'src/entra_auth_desktop.dart'
    if (dart.library.io) 'src/entra_auth_desktop.io.dart';
import 'src/gen/ffi.pb.dart';
import 'src/types.dart';

class Auth {
  final String clientId;
  final String tenantId;

  Auth({required this.clientId, required this.tenantId});

  Guid? _cachedConnection;

  Future<Guid> get connection async {
    return _cachedConnection ??= await newPublicClient(NewPublicClientRequest(
      clientId: clientId,
      tenantId: tenantId,
    ));
  }

  Future<void> dispose() async {
    var cc = _cachedConnection;
    if (cc == null) {
      return;
    }
    _cachedConnection = null;
    await disposePublicClient(DisposePublicClientRequest(clientId: cc));
  }

  Future<void> signout() async {
    final cc = await connection;
    await goSignout(
      SignoutRequest(clientId: cc),
    );
  }

  Future<String> getCachedAccessToken(List<String> scopes) async {
    final cc = await connection;
    final r = await goGetCachedAccessToken(
      GetTokenRequest(
        scopes: scopes,
        clientId: cc,
      ),
    );
    return r.token;
  }

  Future<String> acquireTokenInteractive(List<String> scopes) async {
    final cc = await connection;
    final r = await goAcquireTokenInteractive(
      GetTokenRequest(
        scopes: scopes,
        clientId: cc,
      ),
    );
    return r.token;
  }
}
