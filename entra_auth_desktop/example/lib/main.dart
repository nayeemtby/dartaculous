import 'package:flutter/material.dart';

import 'package:entra_auth_desktop/entra_auth_desktop.dart';

const scopes = ["openid", "profile", "email"];

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String output = '';

  @override
  void initState() {
    super.initState();
    setState(() {
      output = 'waiting for sign in';
    });
  }

  @override
  Widget build(BuildContext context) {
    final a = Auth(
      clientId: '<fill in>',
      tenantId: '<fill in>',
    );
    final f = _getToken(a);
    f.then((value) {
      setState(() {
        output = value;
      });
    });
    const textStyle = TextStyle(fontSize: 25);
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  output,
                  style: textStyle,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<String> _getToken(Auth a) async {
    final tkn = await a.getCachedAccessToken(scopes);
    if (tkn.isNotEmpty) {
      return tkn;
    }
    return await a.acquireTokenInteractive(scopes);
  }
}
