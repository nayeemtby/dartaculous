package helpers

import (
	"com.squarealfa/cp/ffi_proto"
	"errors"
	"github.com/google/uuid"
	"gitlab.com/squarealfa/dart_bridge/ffi"
	"gitlab.com/squarealfa/dart_bridge/stubs"
)

var ErrIdNotFound = errors.New("id not found")

func SendErrorMessage(port int64, err error) {
	mError := ffi_proto.FfiError{
		Message:   err.Error(),
		ErrorType: ffi_proto.ErrorType_unspecified,
	}

	if errors.Is(err, ErrIdNotFound) {
		mError.ErrorType = ffi_proto.ErrorType_id_not_found ///
	}

	ffi.SendMessage(port, &mError)
}

func SendUUID(port int64, value uuid.UUID) {
	response := &stubs.ByteArrayMessage{}
	response.Value = value[:]
	ffi.SendMessage(port, response)
}

func BytesToUuid(bytes []byte) uuid.UUID {
	var oid uuid.UUID
	copy(oid[:], bytes)
	return oid
}
