package helpers

import (
	"context"
	"errors"
	"os"
	"path"

	"github.com/AzureAD/microsoft-authentication-library-for-go/apps/cache"
)

type TokenCache struct {
	file string
}

func NewTokenCache() (*TokenCache, error) {
	ucd, err := os.UserCacheDir()
	if err != nil {
		return nil, err
	}
	dirPath := path.Join(ucd, "euroauctions-stock")
	if err = os.MkdirAll(dirPath, os.ModePerm); err != nil {
		return nil, err
	}

	filePath := path.Join(dirPath, "tkn.json")

	tc := TokenCache{
		file: filePath,
	}
	return &tc, nil
}

func (t TokenCache) Replace(ctx context.Context, cache cache.Unmarshaler, hints cache.ReplaceHints) error {

	if _, err := os.Stat(t.file); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}
		return err
	}
	data, err := os.ReadFile(t.file)
	if err != nil {
		return err
	}
	return cache.Unmarshal(data)
}

func (t TokenCache) Export(ctx context.Context, cache cache.Marshaler, hints cache.ExportHints) error {
	data, err := cache.Marshal()
	if err != nil {
		return err
	}
	return os.WriteFile(t.file, data, 0600)
}
