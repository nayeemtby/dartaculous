# The Flutter tooling requires that developers have CMake 3.10 or later
# installed. You should not increase this version, as doing so will cause
# the plugin to fail to compile for some customers of the plugin.
cmake_minimum_required(VERSION 3.18.4)

project(entra_auth_desktop_library VERSION 0.0.1 LANGUAGES C)


set(TARGET shim_go)

set(SRCS auth.go go.mod go.sum ffi_proto/ffi.pb.go helpers/helpers.go)
IF (WIN32)
  set(LIB adder.dll)
ELSE()
  set(LIB adder.so)
ENDIF()


add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${LIB}
  DEPENDS ${SRCS}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  COMMAND env GOPATH=${GOPATH} go build -buildmode=c-shared
  -o "${CMAKE_CURRENT_BINARY_DIR}/${LIB}"
  ${CMAKE_GO_FLAGS} auth.go
  COMMENT "Building Go library")

add_custom_target(${TARGET} ALL DEPENDS ${LIB} ${HEADER})
add_library(entra_auth_desktop SHARED IMPORTED GLOBAL)
add_dependencies(entra_auth_desktop ${TARGET})
set_target_properties(entra_auth_desktop
  PROPERTIES
  IMPORTED_LOCATION ${CMAKE_CURRENT_BINARY_DIR}/${LIB}
  INTERFACE_INCLUDE_DIRECTORIES ${CMAKE_CURRENT_BINARY_DIR}
)
