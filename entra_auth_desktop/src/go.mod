module com.squarealfa/cp

go 1.21.4

require (
	github.com/AzureAD/microsoft-authentication-library-for-go v1.2.0
	github.com/google/uuid v1.5.0
	gitlab.com/squarealfa/dart_bridge v1.0.2
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/golang-jwt/jwt/v5 v5.0.0 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8 // indirect
	golang.org/x/sys v0.5.0 // indirect
)
