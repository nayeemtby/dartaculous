package main

import (
	"C"
	"context"
	"unsafe"

	"sync"

	"com.squarealfa/cp/ffi_proto"
	"com.squarealfa/cp/helpers"
	"github.com/AzureAD/microsoft-authentication-library-for-go/apps/public"
	"github.com/google/uuid"
	"gitlab.com/squarealfa/dart_bridge/ffi"
)

func main() {}

type publicClient struct {
	public.Client
	tenantId string
}

var clients = make(map[uuid.UUID]publicClient)

var isInitialized bool = false
var isInitializedLock sync.RWMutex

//export initialize
func initialize(api unsafe.Pointer) {
	isInitializedLock.Lock()
	defer isInitializedLock.Unlock()

	if isInitialized {
		return
	}
	isInitialized = true

	ffi.Init(api)
}

//export newPublicClient
func newPublicClient(port int64, buffer *C.uchar, size int) {
	request := &ffi_proto.NewPublicClientRequest{}
	ffi.Unmarshal(unsafe.Pointer(buffer), size, request)

	go func() {
		id, err := uuid.NewUUID()
		if err != nil {
			helpers.SendErrorMessage(port, err)
			return
		}

		tkc, err := helpers.NewTokenCache()
		if err != nil {
			helpers.SendErrorMessage(port, err)
			return
		}

		options := []public.Option{
			public.WithCache(tkc),
		}
		publicClientApp, err := public.New(request.ClientId, options...)
		if err != nil {
			helpers.SendErrorMessage(port, err)
			return
		}

		clients[id] = publicClient{
			Client:   publicClientApp,
			tenantId: request.TenantId,
		}
		helpers.SendUUID(port, id)
	}()

}

//export disposePublicClient
func disposePublicClient(port int64, buffer *C.uchar, size int) {
	var request ffi_proto.DisposePublicClientRequest
	ffi.Unmarshal(unsafe.Pointer(buffer), size, &request)

	go func() {
		id := helpers.BytesToUuid(request.ClientId)
		delete(clients, id)
		ffi.SendEmptyMessage(port)
	}()
}

//export getCachedAccessToken
func getCachedAccessToken(port int64, buffer *C.uchar, size int) {
	var request ffi_proto.GetTokenRequest
	ffi.Unmarshal(unsafe.Pointer(buffer), size, &request)

	go func() {
		id := helpers.BytesToUuid(request.ClientId)
		scopes := request.Scopes
		publicClientApp, ok := clients[id]
		if !ok {
			helpers.SendErrorMessage(port, helpers.ErrIdNotFound)
			return
		}
		ctx := context.Background()
		accounts, err := publicClientApp.Accounts(ctx)
		if err != nil {
			helpers.SendErrorMessage(port, err)
			return
		}

		var tr = &ffi_proto.TokenResponse{Token: ""}
		if len(accounts) == 0 {
			ffi.SendMessage(port, tr)
			return
		}
		ua := accounts[0]
		opts := []public.AcquireSilentOption{
			public.WithSilentAccount(ua),
		}
		if publicClientApp.tenantId != "" {
			opts = append(opts, public.WithTenantID(publicClientApp.tenantId))
		}
		result, err := publicClientApp.AcquireTokenSilent(ctx, scopes, opts...)
		if err != nil {
			helpers.SendErrorMessage(port, err)
		}
		tr.Token = result.IDToken.RawToken
		ffi.SendMessage(port, tr)
	}()
}

//export signout
func signout(port int64, buffer *C.uchar, size int) {
	var request ffi_proto.SignoutRequest
	ffi.Unmarshal(unsafe.Pointer(buffer), size, &request)

	go func() {
		id := helpers.BytesToUuid(request.ClientId)
		publicClientApp, ok := clients[id]
		if !ok {
			helpers.SendErrorMessage(port, helpers.ErrIdNotFound)
			return
		}
		ctx := context.Background()
		accounts, err := publicClientApp.Accounts(ctx)
		if err != nil {
			helpers.SendErrorMessage(port, err)
			return
		}

		if len(accounts) != 0 {
			if err = publicClientApp.RemoveAccount(ctx, accounts[0]); err != nil {
				helpers.SendErrorMessage(port, err)
				return
			}
		}

		ffi.SendEmptyMessage(port)
	}()
}

//export acquireTokenInteractive
func acquireTokenInteractive(port int64, buffer *C.uchar, size int) {
	var request ffi_proto.GetTokenRequest
	ffi.Unmarshal(unsafe.Pointer(buffer), size, &request)

	go func() {
		id := helpers.BytesToUuid(request.ClientId)
		scopes := request.Scopes
		publicClientApp, ok := clients[id]
		if !ok {
			helpers.SendErrorMessage(port, helpers.ErrIdNotFound)
			return
		}
		ctx := context.Background()

		opts := []public.AcquireInteractiveOption{}
		if publicClientApp.tenantId != "" {
			opts = append(opts, public.WithTenantID(publicClientApp.tenantId))
		}
		result, err := publicClientApp.AcquireTokenInteractive(ctx, scopes, opts...)
		if err != nil {
			helpers.SendErrorMessage(port, err)
			return
		}
		var tr = &ffi_proto.TokenResponse{Token: result.IDToken.RawToken}
		ffi.SendMessage(port, tr)
	}()

}
