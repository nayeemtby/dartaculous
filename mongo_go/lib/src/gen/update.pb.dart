//
//  Generated code. Do not modify.
//  source: update.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class UpdateRequest extends $pb.GeneratedMessage {
  factory UpdateRequest({
    $core.List<$core.int>? collectionOid,
    $core.List<$core.int>? sessionOid,
    $core.List<$core.int>? filter,
    $core.List<$core.int>? update,
    $core.bool? isUpsert,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    if (sessionOid != null) {
      $result.sessionOid = sessionOid;
    }
    if (filter != null) {
      $result.filter = filter;
    }
    if (update != null) {
      $result.update = update;
    }
    if (isUpsert != null) {
      $result.isUpsert = isUpsert;
    }
    return $result;
  }
  UpdateRequest._() : super();
  factory UpdateRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory UpdateRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'UpdateRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'sessionOid', $pb.PbFieldType.OY,
        protoName: 'sessionOid')
    ..a<$core.List<$core.int>>(
        3, _omitFieldNames ? '' : 'filter', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        4, _omitFieldNames ? '' : 'update', $pb.PbFieldType.OY)
    ..aOB(5, _omitFieldNames ? '' : 'isUpsert', protoName: 'isUpsert')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  UpdateRequest clone() => UpdateRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  UpdateRequest copyWith(void Function(UpdateRequest) updates) =>
      super.copyWith((message) => updates(message as UpdateRequest))
          as UpdateRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static UpdateRequest create() => UpdateRequest._();
  UpdateRequest createEmptyInstance() => create();
  static $pb.PbList<UpdateRequest> createRepeated() =>
      $pb.PbList<UpdateRequest>();
  @$core.pragma('dart2js:noInline')
  static UpdateRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<UpdateRequest>(create);
  static UpdateRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get sessionOid => $_getN(1);
  @$pb.TagNumber(2)
  set sessionOid($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSessionOid() => $_has(1);
  @$pb.TagNumber(2)
  void clearSessionOid() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get filter => $_getN(2);
  @$pb.TagNumber(3)
  set filter($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasFilter() => $_has(2);
  @$pb.TagNumber(3)
  void clearFilter() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get update => $_getN(3);
  @$pb.TagNumber(4)
  set update($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasUpdate() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdate() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get isUpsert => $_getBF(4);
  @$pb.TagNumber(5)
  set isUpsert($core.bool v) {
    $_setBool(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasIsUpsert() => $_has(4);
  @$pb.TagNumber(5)
  void clearIsUpsert() => clearField(5);
}

class UpdateResult extends $pb.GeneratedMessage {
  factory UpdateResult({
    $fixnum.Int64? matchedCount,
    $fixnum.Int64? modifiedCount,
    $fixnum.Int64? upsertedCount,
    $core.List<$core.int>? upsertedId,
  }) {
    final $result = create();
    if (matchedCount != null) {
      $result.matchedCount = matchedCount;
    }
    if (modifiedCount != null) {
      $result.modifiedCount = modifiedCount;
    }
    if (upsertedCount != null) {
      $result.upsertedCount = upsertedCount;
    }
    if (upsertedId != null) {
      $result.upsertedId = upsertedId;
    }
    return $result;
  }
  UpdateResult._() : super();
  factory UpdateResult.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory UpdateResult.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'UpdateResult',
      createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'matchedCount')
    ..aInt64(2, _omitFieldNames ? '' : 'modifiedCount')
    ..aInt64(3, _omitFieldNames ? '' : 'upsertedCount')
    ..a<$core.List<$core.int>>(
        4, _omitFieldNames ? '' : 'upsertedId', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  UpdateResult clone() => UpdateResult()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  UpdateResult copyWith(void Function(UpdateResult) updates) =>
      super.copyWith((message) => updates(message as UpdateResult))
          as UpdateResult;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static UpdateResult create() => UpdateResult._();
  UpdateResult createEmptyInstance() => create();
  static $pb.PbList<UpdateResult> createRepeated() =>
      $pb.PbList<UpdateResult>();
  @$core.pragma('dart2js:noInline')
  static UpdateResult getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<UpdateResult>(create);
  static UpdateResult? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get matchedCount => $_getI64(0);
  @$pb.TagNumber(1)
  set matchedCount($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasMatchedCount() => $_has(0);
  @$pb.TagNumber(1)
  void clearMatchedCount() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get modifiedCount => $_getI64(1);
  @$pb.TagNumber(2)
  set modifiedCount($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasModifiedCount() => $_has(1);
  @$pb.TagNumber(2)
  void clearModifiedCount() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get upsertedCount => $_getI64(2);
  @$pb.TagNumber(3)
  set upsertedCount($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasUpsertedCount() => $_has(2);
  @$pb.TagNumber(3)
  void clearUpsertedCount() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get upsertedId => $_getN(3);
  @$pb.TagNumber(4)
  set upsertedId($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasUpsertedId() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpsertedId() => clearField(4);
}

const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames =
    $core.bool.fromEnvironment('protobuf.omit_message_names');
