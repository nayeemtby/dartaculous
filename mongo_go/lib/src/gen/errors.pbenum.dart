//
//  Generated code. Do not modify.
//  source: errors.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class ErrorType extends $pb.ProtobufEnum {
  static const ErrorType unspecified =
      ErrorType._(0, _omitEnumNames ? '' : 'unspecified');
  static const ErrorType duplicate_key =
      ErrorType._(1, _omitEnumNames ? '' : 'duplicate_key');
  static const ErrorType network =
      ErrorType._(2, _omitEnumNames ? '' : 'network');
  static const ErrorType timeout =
      ErrorType._(3, _omitEnumNames ? '' : 'timeout');
  static const ErrorType no_documents =
      ErrorType._(4, _omitEnumNames ? '' : 'no_documents');

  static const $core.List<ErrorType> values = <ErrorType>[
    unspecified,
    duplicate_key,
    network,
    timeout,
    no_documents,
  ];

  static final $core.Map<$core.int, ErrorType> _byValue =
      $pb.ProtobufEnum.initByValue(values);
  static ErrorType? valueOf($core.int value) => _byValue[value];

  const ErrorType._($core.int v, $core.String n) : super(v, n);
}

const _omitEnumNames = $core.bool.fromEnvironment('protobuf.omit_enum_names');
