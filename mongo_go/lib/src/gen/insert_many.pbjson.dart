//
//  Generated code. Do not modify.
//  source: insert_many.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use insertManyRequestDescriptor instead')
const InsertManyRequest$json = {
  '1': 'InsertManyRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
    {'1': 'sessionOid', '3': 2, '4': 1, '5': 12, '10': 'sessionOid'},
    {'1': 'documents', '3': 3, '4': 3, '5': 12, '10': 'documents'},
  ],
};

/// Descriptor for `InsertManyRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List insertManyRequestDescriptor = $convert.base64Decode(
    'ChFJbnNlcnRNYW55UmVxdWVzdBIkCg1jb2xsZWN0aW9uT2lkGAEgASgMUg1jb2xsZWN0aW9uT2'
    'lkEh4KCnNlc3Npb25PaWQYAiABKAxSCnNlc3Npb25PaWQSHAoJZG9jdW1lbnRzGAMgAygMUglk'
    'b2N1bWVudHM=');

@$core.Deprecated('Use insertManyResultDescriptor instead')
const InsertManyResult$json = {
  '1': 'InsertManyResult',
  '2': [
    {'1': 'insertedIds', '3': 1, '4': 3, '5': 12, '10': 'insertedIds'},
  ],
};

/// Descriptor for `InsertManyResult`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List insertManyResultDescriptor = $convert.base64Decode(
    'ChBJbnNlcnRNYW55UmVzdWx0EiAKC2luc2VydGVkSWRzGAEgAygMUgtpbnNlcnRlZElkcw==');
