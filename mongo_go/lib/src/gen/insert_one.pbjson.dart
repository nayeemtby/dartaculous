//
//  Generated code. Do not modify.
//  source: insert_one.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use insertOneRequestDescriptor instead')
const InsertOneRequest$json = {
  '1': 'InsertOneRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
    {'1': 'sessionOid', '3': 2, '4': 1, '5': 12, '10': 'sessionOid'},
    {'1': 'document', '3': 3, '4': 1, '5': 12, '10': 'document'},
  ],
};

/// Descriptor for `InsertOneRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List insertOneRequestDescriptor = $convert.base64Decode(
    'ChBJbnNlcnRPbmVSZXF1ZXN0EiQKDWNvbGxlY3Rpb25PaWQYASABKAxSDWNvbGxlY3Rpb25PaW'
    'QSHgoKc2Vzc2lvbk9pZBgCIAEoDFIKc2Vzc2lvbk9pZBIaCghkb2N1bWVudBgDIAEoDFIIZG9j'
    'dW1lbnQ=');

@$core.Deprecated('Use insertOneResultDescriptor instead')
const InsertOneResult$json = {
  '1': 'InsertOneResult',
  '2': [
    {'1': 'insertedId', '3': 1, '4': 1, '5': 12, '10': 'insertedId'},
  ],
};

/// Descriptor for `InsertOneResult`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List insertOneResultDescriptor = $convert.base64Decode(
    'Cg9JbnNlcnRPbmVSZXN1bHQSHgoKaW5zZXJ0ZWRJZBgBIAEoDFIKaW5zZXJ0ZWRJZA==');
