//
//  Generated code. Do not modify.
//  source: database_request.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class DatabaseRequest extends $pb.GeneratedMessage {
  factory DatabaseRequest({
    $core.List<$core.int>? connectionOid,
    $core.String? databaseName,
  }) {
    final $result = create();
    if (connectionOid != null) {
      $result.connectionOid = connectionOid;
    }
    if (databaseName != null) {
      $result.databaseName = databaseName;
    }
    return $result;
  }
  DatabaseRequest._() : super();
  factory DatabaseRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DatabaseRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'DatabaseRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'connectionOid', $pb.PbFieldType.OY,
        protoName: 'connectionOid')
    ..aOS(2, _omitFieldNames ? '' : 'databaseName', protoName: 'databaseName')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  DatabaseRequest clone() => DatabaseRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  DatabaseRequest copyWith(void Function(DatabaseRequest) updates) =>
      super.copyWith((message) => updates(message as DatabaseRequest))
          as DatabaseRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DatabaseRequest create() => DatabaseRequest._();
  DatabaseRequest createEmptyInstance() => create();
  static $pb.PbList<DatabaseRequest> createRepeated() =>
      $pb.PbList<DatabaseRequest>();
  @$core.pragma('dart2js:noInline')
  static DatabaseRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DatabaseRequest>(create);
  static DatabaseRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get connectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set connectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasConnectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearConnectionOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get databaseName => $_getSZ(1);
  @$pb.TagNumber(2)
  set databaseName($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasDatabaseName() => $_has(1);
  @$pb.TagNumber(2)
  void clearDatabaseName() => clearField(2);
}

const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames =
    $core.bool.fromEnvironment('protobuf.omit_message_names');
