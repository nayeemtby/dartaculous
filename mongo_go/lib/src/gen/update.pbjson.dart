//
//  Generated code. Do not modify.
//  source: update.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use updateRequestDescriptor instead')
const UpdateRequest$json = {
  '1': 'UpdateRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
    {'1': 'sessionOid', '3': 2, '4': 1, '5': 12, '10': 'sessionOid'},
    {'1': 'filter', '3': 3, '4': 1, '5': 12, '10': 'filter'},
    {'1': 'update', '3': 4, '4': 1, '5': 12, '10': 'update'},
    {'1': 'isUpsert', '3': 5, '4': 1, '5': 8, '10': 'isUpsert'},
  ],
};

/// Descriptor for `UpdateRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateRequestDescriptor = $convert.base64Decode(
    'Cg1VcGRhdGVSZXF1ZXN0EiQKDWNvbGxlY3Rpb25PaWQYASABKAxSDWNvbGxlY3Rpb25PaWQSHg'
    'oKc2Vzc2lvbk9pZBgCIAEoDFIKc2Vzc2lvbk9pZBIWCgZmaWx0ZXIYAyABKAxSBmZpbHRlchIW'
    'CgZ1cGRhdGUYBCABKAxSBnVwZGF0ZRIaCghpc1Vwc2VydBgFIAEoCFIIaXNVcHNlcnQ=');

@$core.Deprecated('Use updateResultDescriptor instead')
const UpdateResult$json = {
  '1': 'UpdateResult',
  '2': [
    {'1': 'matched_count', '3': 1, '4': 1, '5': 3, '10': 'matchedCount'},
    {'1': 'modified_count', '3': 2, '4': 1, '5': 3, '10': 'modifiedCount'},
    {'1': 'upserted_count', '3': 3, '4': 1, '5': 3, '10': 'upsertedCount'},
    {'1': 'upserted_id', '3': 4, '4': 1, '5': 12, '10': 'upsertedId'},
  ],
};

/// Descriptor for `UpdateResult`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateResultDescriptor = $convert.base64Decode(
    'CgxVcGRhdGVSZXN1bHQSIwoNbWF0Y2hlZF9jb3VudBgBIAEoA1IMbWF0Y2hlZENvdW50EiUKDm'
    '1vZGlmaWVkX2NvdW50GAIgASgDUg1tb2RpZmllZENvdW50EiUKDnVwc2VydGVkX2NvdW50GAMg'
    'ASgDUg11cHNlcnRlZENvdW50Eh8KC3Vwc2VydGVkX2lkGAQgASgMUgp1cHNlcnRlZElk');
