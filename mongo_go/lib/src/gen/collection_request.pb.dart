//
//  Generated code. Do not modify.
//  source: collection_request.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class CollectionRequest extends $pb.GeneratedMessage {
  factory CollectionRequest({
    $core.List<$core.int>? databaseOid,
    $core.String? collectionName,
  }) {
    final $result = create();
    if (databaseOid != null) {
      $result.databaseOid = databaseOid;
    }
    if (collectionName != null) {
      $result.collectionName = collectionName;
    }
    return $result;
  }
  CollectionRequest._() : super();
  factory CollectionRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory CollectionRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'CollectionRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'databaseOid', $pb.PbFieldType.OY,
        protoName: 'databaseOid')
    ..aOS(2, _omitFieldNames ? '' : 'collectionName',
        protoName: 'collectionName')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  CollectionRequest clone() => CollectionRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  CollectionRequest copyWith(void Function(CollectionRequest) updates) =>
      super.copyWith((message) => updates(message as CollectionRequest))
          as CollectionRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static CollectionRequest create() => CollectionRequest._();
  CollectionRequest createEmptyInstance() => create();
  static $pb.PbList<CollectionRequest> createRepeated() =>
      $pb.PbList<CollectionRequest>();
  @$core.pragma('dart2js:noInline')
  static CollectionRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<CollectionRequest>(create);
  static CollectionRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get databaseOid => $_getN(0);
  @$pb.TagNumber(1)
  set databaseOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasDatabaseOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearDatabaseOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get collectionName => $_getSZ(1);
  @$pb.TagNumber(2)
  set collectionName($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasCollectionName() => $_has(1);
  @$pb.TagNumber(2)
  void clearCollectionName() => clearField(2);
}

const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames =
    $core.bool.fromEnvironment('protobuf.omit_message_names');
