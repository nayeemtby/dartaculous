//
//  Generated code. Do not modify.
//  source: collection_request.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use collectionRequestDescriptor instead')
const CollectionRequest$json = {
  '1': 'CollectionRequest',
  '2': [
    {'1': 'databaseOid', '3': 1, '4': 1, '5': 12, '10': 'databaseOid'},
    {'1': 'collectionName', '3': 2, '4': 1, '5': 9, '10': 'collectionName'},
  ],
};

/// Descriptor for `CollectionRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List collectionRequestDescriptor = $convert.base64Decode(
    'ChFDb2xsZWN0aW9uUmVxdWVzdBIgCgtkYXRhYmFzZU9pZBgBIAEoDFILZGF0YWJhc2VPaWQSJg'
    'oOY29sbGVjdGlvbk5hbWUYAiABKAlSDmNvbGxlY3Rpb25OYW1l');
