//
//  Generated code. Do not modify.
//  source: index_requests.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'google/protobuf/wrappers.pb.dart' as $3;

class Collation extends $pb.GeneratedMessage {
  factory Collation({
    $core.String? locale,
    $core.bool? caseLevel,
    $core.String? caseFirst,
    $core.int? strength,
    $core.bool? numericOrdering,
    $core.String? alternate,
    $core.String? maxVariable,
    $core.bool? normalization,
    $core.bool? backwards,
  }) {
    final $result = create();
    if (locale != null) {
      $result.locale = locale;
    }
    if (caseLevel != null) {
      $result.caseLevel = caseLevel;
    }
    if (caseFirst != null) {
      $result.caseFirst = caseFirst;
    }
    if (strength != null) {
      $result.strength = strength;
    }
    if (numericOrdering != null) {
      $result.numericOrdering = numericOrdering;
    }
    if (alternate != null) {
      $result.alternate = alternate;
    }
    if (maxVariable != null) {
      $result.maxVariable = maxVariable;
    }
    if (normalization != null) {
      $result.normalization = normalization;
    }
    if (backwards != null) {
      $result.backwards = backwards;
    }
    return $result;
  }
  Collation._() : super();
  factory Collation.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory Collation.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'Collation',
      createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'Locale', protoName: 'Locale')
    ..aOB(2, _omitFieldNames ? '' : 'CaseLevel', protoName: 'CaseLevel')
    ..aOS(3, _omitFieldNames ? '' : 'CaseFirst', protoName: 'CaseFirst')
    ..a<$core.int>(4, _omitFieldNames ? '' : 'Strength', $pb.PbFieldType.O3,
        protoName: 'Strength')
    ..aOB(5, _omitFieldNames ? '' : 'NumericOrdering',
        protoName: 'NumericOrdering')
    ..aOS(6, _omitFieldNames ? '' : 'Alternate', protoName: 'Alternate')
    ..aOS(7, _omitFieldNames ? '' : 'MaxVariable', protoName: 'MaxVariable')
    ..aOB(8, _omitFieldNames ? '' : 'Normalization', protoName: 'Normalization')
    ..aOB(9, _omitFieldNames ? '' : 'Backwards', protoName: 'Backwards')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  Collation clone() => Collation()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  Collation copyWith(void Function(Collation) updates) =>
      super.copyWith((message) => updates(message as Collation)) as Collation;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static Collation create() => Collation._();
  Collation createEmptyInstance() => create();
  static $pb.PbList<Collation> createRepeated() => $pb.PbList<Collation>();
  @$core.pragma('dart2js:noInline')
  static Collation getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Collation>(create);
  static Collation? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get locale => $_getSZ(0);
  @$pb.TagNumber(1)
  set locale($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasLocale() => $_has(0);
  @$pb.TagNumber(1)
  void clearLocale() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get caseLevel => $_getBF(1);
  @$pb.TagNumber(2)
  set caseLevel($core.bool v) {
    $_setBool(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasCaseLevel() => $_has(1);
  @$pb.TagNumber(2)
  void clearCaseLevel() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get caseFirst => $_getSZ(2);
  @$pb.TagNumber(3)
  set caseFirst($core.String v) {
    $_setString(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasCaseFirst() => $_has(2);
  @$pb.TagNumber(3)
  void clearCaseFirst() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get strength => $_getIZ(3);
  @$pb.TagNumber(4)
  set strength($core.int v) {
    $_setSignedInt32(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasStrength() => $_has(3);
  @$pb.TagNumber(4)
  void clearStrength() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get numericOrdering => $_getBF(4);
  @$pb.TagNumber(5)
  set numericOrdering($core.bool v) {
    $_setBool(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasNumericOrdering() => $_has(4);
  @$pb.TagNumber(5)
  void clearNumericOrdering() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get alternate => $_getSZ(5);
  @$pb.TagNumber(6)
  set alternate($core.String v) {
    $_setString(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasAlternate() => $_has(5);
  @$pb.TagNumber(6)
  void clearAlternate() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get maxVariable => $_getSZ(6);
  @$pb.TagNumber(7)
  set maxVariable($core.String v) {
    $_setString(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasMaxVariable() => $_has(6);
  @$pb.TagNumber(7)
  void clearMaxVariable() => clearField(7);

  @$pb.TagNumber(8)
  $core.bool get normalization => $_getBF(7);
  @$pb.TagNumber(8)
  set normalization($core.bool v) {
    $_setBool(7, v);
  }

  @$pb.TagNumber(8)
  $core.bool hasNormalization() => $_has(7);
  @$pb.TagNumber(8)
  void clearNormalization() => clearField(8);

  @$pb.TagNumber(9)
  $core.bool get backwards => $_getBF(8);
  @$pb.TagNumber(9)
  set backwards($core.bool v) {
    $_setBool(8, v);
  }

  @$pb.TagNumber(9)
  $core.bool hasBackwards() => $_has(8);
  @$pb.TagNumber(9)
  void clearBackwards() => clearField(9);
}

class IndexOptions extends $pb.GeneratedMessage {
  factory IndexOptions({
    $3.BoolValue? background,
    $3.Int32Value? expireAfterSeconds,
    $3.StringValue? name,
    $3.BoolValue? sparse,
    $core.List<$core.int>? storageEngine,
    $3.BoolValue? unique,
    $3.Int32Value? version,
    $3.StringValue? defaultLanguage,
    $3.StringValue? languageOverride,
    $3.Int32Value? textVersion,
    $core.List<$core.int>? weights,
    $3.Int32Value? sphereVersion,
    $3.Int32Value? bits,
    $3.DoubleValue? max,
    $3.DoubleValue? min,
    $3.Int32Value? bucketSize,
    $core.List<$core.int>? partialFilterExpression,
    $core.List<$core.int>? collation,
    $core.List<$core.int>? wildcardProjection,
    $3.BoolValue? hidden,
  }) {
    final $result = create();
    if (background != null) {
      $result.background = background;
    }
    if (expireAfterSeconds != null) {
      $result.expireAfterSeconds = expireAfterSeconds;
    }
    if (name != null) {
      $result.name = name;
    }
    if (sparse != null) {
      $result.sparse = sparse;
    }
    if (storageEngine != null) {
      $result.storageEngine = storageEngine;
    }
    if (unique != null) {
      $result.unique = unique;
    }
    if (version != null) {
      $result.version = version;
    }
    if (defaultLanguage != null) {
      $result.defaultLanguage = defaultLanguage;
    }
    if (languageOverride != null) {
      $result.languageOverride = languageOverride;
    }
    if (textVersion != null) {
      $result.textVersion = textVersion;
    }
    if (weights != null) {
      $result.weights = weights;
    }
    if (sphereVersion != null) {
      $result.sphereVersion = sphereVersion;
    }
    if (bits != null) {
      $result.bits = bits;
    }
    if (max != null) {
      $result.max = max;
    }
    if (min != null) {
      $result.min = min;
    }
    if (bucketSize != null) {
      $result.bucketSize = bucketSize;
    }
    if (partialFilterExpression != null) {
      $result.partialFilterExpression = partialFilterExpression;
    }
    if (collation != null) {
      $result.collation = collation;
    }
    if (wildcardProjection != null) {
      $result.wildcardProjection = wildcardProjection;
    }
    if (hidden != null) {
      $result.hidden = hidden;
    }
    return $result;
  }
  IndexOptions._() : super();
  factory IndexOptions.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory IndexOptions.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'IndexOptions',
      createEmptyInstance: create)
    ..aOM<$3.BoolValue>(1, _omitFieldNames ? '' : 'background',
        subBuilder: $3.BoolValue.create)
    ..aOM<$3.Int32Value>(2, _omitFieldNames ? '' : 'expireAfterSeconds',
        protoName: 'expireAfterSeconds', subBuilder: $3.Int32Value.create)
    ..aOM<$3.StringValue>(3, _omitFieldNames ? '' : 'name',
        subBuilder: $3.StringValue.create)
    ..aOM<$3.BoolValue>(4, _omitFieldNames ? '' : 'sparse',
        subBuilder: $3.BoolValue.create)
    ..a<$core.List<$core.int>>(
        5, _omitFieldNames ? '' : 'storageEngine', $pb.PbFieldType.OY,
        protoName: 'storageEngine')
    ..aOM<$3.BoolValue>(6, _omitFieldNames ? '' : 'unique',
        subBuilder: $3.BoolValue.create)
    ..aOM<$3.Int32Value>(7, _omitFieldNames ? '' : 'version',
        subBuilder: $3.Int32Value.create)
    ..aOM<$3.StringValue>(8, _omitFieldNames ? '' : 'defaultLanguage',
        protoName: 'defaultLanguage', subBuilder: $3.StringValue.create)
    ..aOM<$3.StringValue>(9, _omitFieldNames ? '' : 'languageOverride',
        protoName: 'languageOverride', subBuilder: $3.StringValue.create)
    ..aOM<$3.Int32Value>(10, _omitFieldNames ? '' : 'textVersion',
        protoName: 'textVersion', subBuilder: $3.Int32Value.create)
    ..a<$core.List<$core.int>>(
        11, _omitFieldNames ? '' : 'weights', $pb.PbFieldType.OY)
    ..aOM<$3.Int32Value>(12, _omitFieldNames ? '' : 'sphereVersion',
        protoName: 'sphereVersion', subBuilder: $3.Int32Value.create)
    ..aOM<$3.Int32Value>(13, _omitFieldNames ? '' : 'bits',
        subBuilder: $3.Int32Value.create)
    ..aOM<$3.DoubleValue>(14, _omitFieldNames ? '' : 'max',
        subBuilder: $3.DoubleValue.create)
    ..aOM<$3.DoubleValue>(15, _omitFieldNames ? '' : 'min',
        subBuilder: $3.DoubleValue.create)
    ..aOM<$3.Int32Value>(16, _omitFieldNames ? '' : 'bucketSize',
        protoName: 'bucketSize', subBuilder: $3.Int32Value.create)
    ..a<$core.List<$core.int>>(17,
        _omitFieldNames ? '' : 'partialFilterExpression', $pb.PbFieldType.OY,
        protoName: 'partialFilterExpression')
    ..a<$core.List<$core.int>>(
        18, _omitFieldNames ? '' : 'collation', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        19, _omitFieldNames ? '' : 'wildcardProjection', $pb.PbFieldType.OY,
        protoName: 'wildcardProjection')
    ..aOM<$3.BoolValue>(20, _omitFieldNames ? '' : 'hidden',
        subBuilder: $3.BoolValue.create)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  IndexOptions clone() => IndexOptions()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  IndexOptions copyWith(void Function(IndexOptions) updates) =>
      super.copyWith((message) => updates(message as IndexOptions))
          as IndexOptions;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static IndexOptions create() => IndexOptions._();
  IndexOptions createEmptyInstance() => create();
  static $pb.PbList<IndexOptions> createRepeated() =>
      $pb.PbList<IndexOptions>();
  @$core.pragma('dart2js:noInline')
  static IndexOptions getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<IndexOptions>(create);
  static IndexOptions? _defaultInstance;

  @$pb.TagNumber(1)
  $3.BoolValue get background => $_getN(0);
  @$pb.TagNumber(1)
  set background($3.BoolValue v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasBackground() => $_has(0);
  @$pb.TagNumber(1)
  void clearBackground() => clearField(1);
  @$pb.TagNumber(1)
  $3.BoolValue ensureBackground() => $_ensure(0);

  @$pb.TagNumber(2)
  $3.Int32Value get expireAfterSeconds => $_getN(1);
  @$pb.TagNumber(2)
  set expireAfterSeconds($3.Int32Value v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasExpireAfterSeconds() => $_has(1);
  @$pb.TagNumber(2)
  void clearExpireAfterSeconds() => clearField(2);
  @$pb.TagNumber(2)
  $3.Int32Value ensureExpireAfterSeconds() => $_ensure(1);

  @$pb.TagNumber(3)
  $3.StringValue get name => $_getN(2);
  @$pb.TagNumber(3)
  set name($3.StringValue v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);
  @$pb.TagNumber(3)
  $3.StringValue ensureName() => $_ensure(2);

  @$pb.TagNumber(4)
  $3.BoolValue get sparse => $_getN(3);
  @$pb.TagNumber(4)
  set sparse($3.BoolValue v) {
    setField(4, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasSparse() => $_has(3);
  @$pb.TagNumber(4)
  void clearSparse() => clearField(4);
  @$pb.TagNumber(4)
  $3.BoolValue ensureSparse() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.List<$core.int> get storageEngine => $_getN(4);
  @$pb.TagNumber(5)
  set storageEngine($core.List<$core.int> v) {
    $_setBytes(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasStorageEngine() => $_has(4);
  @$pb.TagNumber(5)
  void clearStorageEngine() => clearField(5);

  @$pb.TagNumber(6)
  $3.BoolValue get unique => $_getN(5);
  @$pb.TagNumber(6)
  set unique($3.BoolValue v) {
    setField(6, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasUnique() => $_has(5);
  @$pb.TagNumber(6)
  void clearUnique() => clearField(6);
  @$pb.TagNumber(6)
  $3.BoolValue ensureUnique() => $_ensure(5);

  @$pb.TagNumber(7)
  $3.Int32Value get version => $_getN(6);
  @$pb.TagNumber(7)
  set version($3.Int32Value v) {
    setField(7, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasVersion() => $_has(6);
  @$pb.TagNumber(7)
  void clearVersion() => clearField(7);
  @$pb.TagNumber(7)
  $3.Int32Value ensureVersion() => $_ensure(6);

  @$pb.TagNumber(8)
  $3.StringValue get defaultLanguage => $_getN(7);
  @$pb.TagNumber(8)
  set defaultLanguage($3.StringValue v) {
    setField(8, v);
  }

  @$pb.TagNumber(8)
  $core.bool hasDefaultLanguage() => $_has(7);
  @$pb.TagNumber(8)
  void clearDefaultLanguage() => clearField(8);
  @$pb.TagNumber(8)
  $3.StringValue ensureDefaultLanguage() => $_ensure(7);

  @$pb.TagNumber(9)
  $3.StringValue get languageOverride => $_getN(8);
  @$pb.TagNumber(9)
  set languageOverride($3.StringValue v) {
    setField(9, v);
  }

  @$pb.TagNumber(9)
  $core.bool hasLanguageOverride() => $_has(8);
  @$pb.TagNumber(9)
  void clearLanguageOverride() => clearField(9);
  @$pb.TagNumber(9)
  $3.StringValue ensureLanguageOverride() => $_ensure(8);

  @$pb.TagNumber(10)
  $3.Int32Value get textVersion => $_getN(9);
  @$pb.TagNumber(10)
  set textVersion($3.Int32Value v) {
    setField(10, v);
  }

  @$pb.TagNumber(10)
  $core.bool hasTextVersion() => $_has(9);
  @$pb.TagNumber(10)
  void clearTextVersion() => clearField(10);
  @$pb.TagNumber(10)
  $3.Int32Value ensureTextVersion() => $_ensure(9);

  @$pb.TagNumber(11)
  $core.List<$core.int> get weights => $_getN(10);
  @$pb.TagNumber(11)
  set weights($core.List<$core.int> v) {
    $_setBytes(10, v);
  }

  @$pb.TagNumber(11)
  $core.bool hasWeights() => $_has(10);
  @$pb.TagNumber(11)
  void clearWeights() => clearField(11);

  @$pb.TagNumber(12)
  $3.Int32Value get sphereVersion => $_getN(11);
  @$pb.TagNumber(12)
  set sphereVersion($3.Int32Value v) {
    setField(12, v);
  }

  @$pb.TagNumber(12)
  $core.bool hasSphereVersion() => $_has(11);
  @$pb.TagNumber(12)
  void clearSphereVersion() => clearField(12);
  @$pb.TagNumber(12)
  $3.Int32Value ensureSphereVersion() => $_ensure(11);

  @$pb.TagNumber(13)
  $3.Int32Value get bits => $_getN(12);
  @$pb.TagNumber(13)
  set bits($3.Int32Value v) {
    setField(13, v);
  }

  @$pb.TagNumber(13)
  $core.bool hasBits() => $_has(12);
  @$pb.TagNumber(13)
  void clearBits() => clearField(13);
  @$pb.TagNumber(13)
  $3.Int32Value ensureBits() => $_ensure(12);

  @$pb.TagNumber(14)
  $3.DoubleValue get max => $_getN(13);
  @$pb.TagNumber(14)
  set max($3.DoubleValue v) {
    setField(14, v);
  }

  @$pb.TagNumber(14)
  $core.bool hasMax() => $_has(13);
  @$pb.TagNumber(14)
  void clearMax() => clearField(14);
  @$pb.TagNumber(14)
  $3.DoubleValue ensureMax() => $_ensure(13);

  @$pb.TagNumber(15)
  $3.DoubleValue get min => $_getN(14);
  @$pb.TagNumber(15)
  set min($3.DoubleValue v) {
    setField(15, v);
  }

  @$pb.TagNumber(15)
  $core.bool hasMin() => $_has(14);
  @$pb.TagNumber(15)
  void clearMin() => clearField(15);
  @$pb.TagNumber(15)
  $3.DoubleValue ensureMin() => $_ensure(14);

  @$pb.TagNumber(16)
  $3.Int32Value get bucketSize => $_getN(15);
  @$pb.TagNumber(16)
  set bucketSize($3.Int32Value v) {
    setField(16, v);
  }

  @$pb.TagNumber(16)
  $core.bool hasBucketSize() => $_has(15);
  @$pb.TagNumber(16)
  void clearBucketSize() => clearField(16);
  @$pb.TagNumber(16)
  $3.Int32Value ensureBucketSize() => $_ensure(15);

  @$pb.TagNumber(17)
  $core.List<$core.int> get partialFilterExpression => $_getN(16);
  @$pb.TagNumber(17)
  set partialFilterExpression($core.List<$core.int> v) {
    $_setBytes(16, v);
  }

  @$pb.TagNumber(17)
  $core.bool hasPartialFilterExpression() => $_has(16);
  @$pb.TagNumber(17)
  void clearPartialFilterExpression() => clearField(17);

  @$pb.TagNumber(18)
  $core.List<$core.int> get collation => $_getN(17);
  @$pb.TagNumber(18)
  set collation($core.List<$core.int> v) {
    $_setBytes(17, v);
  }

  @$pb.TagNumber(18)
  $core.bool hasCollation() => $_has(17);
  @$pb.TagNumber(18)
  void clearCollation() => clearField(18);

  @$pb.TagNumber(19)
  $core.List<$core.int> get wildcardProjection => $_getN(18);
  @$pb.TagNumber(19)
  set wildcardProjection($core.List<$core.int> v) {
    $_setBytes(18, v);
  }

  @$pb.TagNumber(19)
  $core.bool hasWildcardProjection() => $_has(18);
  @$pb.TagNumber(19)
  void clearWildcardProjection() => clearField(19);

  @$pb.TagNumber(20)
  $3.BoolValue get hidden => $_getN(19);
  @$pb.TagNumber(20)
  set hidden($3.BoolValue v) {
    setField(20, v);
  }

  @$pb.TagNumber(20)
  $core.bool hasHidden() => $_has(19);
  @$pb.TagNumber(20)
  void clearHidden() => clearField(20);
  @$pb.TagNumber(20)
  $3.BoolValue ensureHidden() => $_ensure(19);
}

class CreateIndexRequest extends $pb.GeneratedMessage {
  factory CreateIndexRequest({
    $core.List<$core.int>? collectionOid,
    $core.List<$core.int>? keys,
    $core.List<$core.int>? indexOptions,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    if (keys != null) {
      $result.keys = keys;
    }
    if (indexOptions != null) {
      $result.indexOptions = indexOptions;
    }
    return $result;
  }
  CreateIndexRequest._() : super();
  factory CreateIndexRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory CreateIndexRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'CreateIndexRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'keys', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        3, _omitFieldNames ? '' : 'indexOptions', $pb.PbFieldType.OY,
        protoName: 'indexOptions')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  CreateIndexRequest clone() => CreateIndexRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  CreateIndexRequest copyWith(void Function(CreateIndexRequest) updates) =>
      super.copyWith((message) => updates(message as CreateIndexRequest))
          as CreateIndexRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static CreateIndexRequest create() => CreateIndexRequest._();
  CreateIndexRequest createEmptyInstance() => create();
  static $pb.PbList<CreateIndexRequest> createRepeated() =>
      $pb.PbList<CreateIndexRequest>();
  @$core.pragma('dart2js:noInline')
  static CreateIndexRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<CreateIndexRequest>(create);
  static CreateIndexRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get keys => $_getN(1);
  @$pb.TagNumber(2)
  set keys($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasKeys() => $_has(1);
  @$pb.TagNumber(2)
  void clearKeys() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get indexOptions => $_getN(2);
  @$pb.TagNumber(3)
  set indexOptions($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasIndexOptions() => $_has(2);
  @$pb.TagNumber(3)
  void clearIndexOptions() => clearField(3);
}

class DropIndexRequest extends $pb.GeneratedMessage {
  factory DropIndexRequest({
    $core.List<$core.int>? collectionOid,
    $core.String? name,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    if (name != null) {
      $result.name = name;
    }
    return $result;
  }
  DropIndexRequest._() : super();
  factory DropIndexRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DropIndexRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'DropIndexRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..aOS(2, _omitFieldNames ? '' : 'name')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  DropIndexRequest clone() => DropIndexRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  DropIndexRequest copyWith(void Function(DropIndexRequest) updates) =>
      super.copyWith((message) => updates(message as DropIndexRequest))
          as DropIndexRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DropIndexRequest create() => DropIndexRequest._();
  DropIndexRequest createEmptyInstance() => create();
  static $pb.PbList<DropIndexRequest> createRepeated() =>
      $pb.PbList<DropIndexRequest>();
  @$core.pragma('dart2js:noInline')
  static DropIndexRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DropIndexRequest>(create);
  static DropIndexRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);
}

class DropAllIndexesRequest extends $pb.GeneratedMessage {
  factory DropAllIndexesRequest({
    $core.List<$core.int>? collectionOid,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    return $result;
  }
  DropAllIndexesRequest._() : super();
  factory DropAllIndexesRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DropAllIndexesRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'DropAllIndexesRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  DropAllIndexesRequest clone() =>
      DropAllIndexesRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  DropAllIndexesRequest copyWith(
          void Function(DropAllIndexesRequest) updates) =>
      super.copyWith((message) => updates(message as DropAllIndexesRequest))
          as DropAllIndexesRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DropAllIndexesRequest create() => DropAllIndexesRequest._();
  DropAllIndexesRequest createEmptyInstance() => create();
  static $pb.PbList<DropAllIndexesRequest> createRepeated() =>
      $pb.PbList<DropAllIndexesRequest>();
  @$core.pragma('dart2js:noInline')
  static DropAllIndexesRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DropAllIndexesRequest>(create);
  static DropAllIndexesRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);
}

class ListIndexesRequest extends $pb.GeneratedMessage {
  factory ListIndexesRequest({
    $core.List<$core.int>? collectionOid,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    return $result;
  }
  ListIndexesRequest._() : super();
  factory ListIndexesRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory ListIndexesRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'ListIndexesRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  ListIndexesRequest clone() => ListIndexesRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  ListIndexesRequest copyWith(void Function(ListIndexesRequest) updates) =>
      super.copyWith((message) => updates(message as ListIndexesRequest))
          as ListIndexesRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static ListIndexesRequest create() => ListIndexesRequest._();
  ListIndexesRequest createEmptyInstance() => create();
  static $pb.PbList<ListIndexesRequest> createRepeated() =>
      $pb.PbList<ListIndexesRequest>();
  @$core.pragma('dart2js:noInline')
  static ListIndexesRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<ListIndexesRequest>(create);
  static ListIndexesRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);
}

const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames =
    $core.bool.fromEnvironment('protobuf.omit_message_names');
