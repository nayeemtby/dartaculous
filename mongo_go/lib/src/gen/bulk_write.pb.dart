//
//  Generated code. Do not modify.
//  source: bulk_write.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'google/protobuf/wrappers.pb.dart' as $3;

enum BulkWriteRequest_Opts { options, notSet }

class BulkWriteRequest extends $pb.GeneratedMessage {
  factory BulkWriteRequest({
    $core.List<$core.int>? collectionOid,
    $core.List<$core.int>? sessionOid,
    $core.Iterable<WriteModel>? writeModels,
    BulkWriteOptions? options,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    if (sessionOid != null) {
      $result.sessionOid = sessionOid;
    }
    if (writeModels != null) {
      $result.writeModels.addAll(writeModels);
    }
    if (options != null) {
      $result.options = options;
    }
    return $result;
  }
  BulkWriteRequest._() : super();
  factory BulkWriteRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory BulkWriteRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static const $core.Map<$core.int, BulkWriteRequest_Opts>
      _BulkWriteRequest_OptsByTag = {
    5: BulkWriteRequest_Opts.options,
    0: BulkWriteRequest_Opts.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'BulkWriteRequest',
      createEmptyInstance: create)
    ..oo(0, [5])
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'sessionOid', $pb.PbFieldType.OY,
        protoName: 'sessionOid')
    ..pc<WriteModel>(
        3, _omitFieldNames ? '' : 'writeModels', $pb.PbFieldType.PM,
        protoName: 'writeModels', subBuilder: WriteModel.create)
    ..aOM<BulkWriteOptions>(5, _omitFieldNames ? '' : 'options',
        subBuilder: BulkWriteOptions.create)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  BulkWriteRequest clone() => BulkWriteRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  BulkWriteRequest copyWith(void Function(BulkWriteRequest) updates) =>
      super.copyWith((message) => updates(message as BulkWriteRequest))
          as BulkWriteRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static BulkWriteRequest create() => BulkWriteRequest._();
  BulkWriteRequest createEmptyInstance() => create();
  static $pb.PbList<BulkWriteRequest> createRepeated() =>
      $pb.PbList<BulkWriteRequest>();
  @$core.pragma('dart2js:noInline')
  static BulkWriteRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<BulkWriteRequest>(create);
  static BulkWriteRequest? _defaultInstance;

  BulkWriteRequest_Opts whichOpts() =>
      _BulkWriteRequest_OptsByTag[$_whichOneof(0)]!;
  void clearOpts() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get sessionOid => $_getN(1);
  @$pb.TagNumber(2)
  set sessionOid($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSessionOid() => $_has(1);
  @$pb.TagNumber(2)
  void clearSessionOid() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<WriteModel> get writeModels => $_getList(2);

  @$pb.TagNumber(5)
  BulkWriteOptions get options => $_getN(3);
  @$pb.TagNumber(5)
  set options(BulkWriteOptions v) {
    setField(5, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasOptions() => $_has(3);
  @$pb.TagNumber(5)
  void clearOptions() => clearField(5);
  @$pb.TagNumber(5)
  BulkWriteOptions ensureOptions() => $_ensure(3);
}

class BulkWriteOptions extends $pb.GeneratedMessage {
  factory BulkWriteOptions({
    $3.BoolValue? bypassDocumentValidation,
    $core.List<$core.int>? comment,
    $3.BoolValue? ordered,
    $core.List<$core.int>? let,
  }) {
    final $result = create();
    if (bypassDocumentValidation != null) {
      $result.bypassDocumentValidation = bypassDocumentValidation;
    }
    if (comment != null) {
      $result.comment = comment;
    }
    if (ordered != null) {
      $result.ordered = ordered;
    }
    if (let != null) {
      $result.let = let;
    }
    return $result;
  }
  BulkWriteOptions._() : super();
  factory BulkWriteOptions.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory BulkWriteOptions.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'BulkWriteOptions',
      createEmptyInstance: create)
    ..aOM<$3.BoolValue>(1, _omitFieldNames ? '' : 'bypassDocumentValidation',
        subBuilder: $3.BoolValue.create)
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'comment', $pb.PbFieldType.OY)
    ..aOM<$3.BoolValue>(3, _omitFieldNames ? '' : 'ordered',
        subBuilder: $3.BoolValue.create)
    ..a<$core.List<$core.int>>(
        4, _omitFieldNames ? '' : 'let', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  BulkWriteOptions clone() => BulkWriteOptions()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  BulkWriteOptions copyWith(void Function(BulkWriteOptions) updates) =>
      super.copyWith((message) => updates(message as BulkWriteOptions))
          as BulkWriteOptions;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static BulkWriteOptions create() => BulkWriteOptions._();
  BulkWriteOptions createEmptyInstance() => create();
  static $pb.PbList<BulkWriteOptions> createRepeated() =>
      $pb.PbList<BulkWriteOptions>();
  @$core.pragma('dart2js:noInline')
  static BulkWriteOptions getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<BulkWriteOptions>(create);
  static BulkWriteOptions? _defaultInstance;

  @$pb.TagNumber(1)
  $3.BoolValue get bypassDocumentValidation => $_getN(0);
  @$pb.TagNumber(1)
  set bypassDocumentValidation($3.BoolValue v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasBypassDocumentValidation() => $_has(0);
  @$pb.TagNumber(1)
  void clearBypassDocumentValidation() => clearField(1);
  @$pb.TagNumber(1)
  $3.BoolValue ensureBypassDocumentValidation() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.int> get comment => $_getN(1);
  @$pb.TagNumber(2)
  set comment($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasComment() => $_has(1);
  @$pb.TagNumber(2)
  void clearComment() => clearField(2);

  @$pb.TagNumber(3)
  $3.BoolValue get ordered => $_getN(2);
  @$pb.TagNumber(3)
  set ordered($3.BoolValue v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasOrdered() => $_has(2);
  @$pb.TagNumber(3)
  void clearOrdered() => clearField(3);
  @$pb.TagNumber(3)
  $3.BoolValue ensureOrdered() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.List<$core.int> get let => $_getN(3);
  @$pb.TagNumber(4)
  set let($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasLet() => $_has(3);
  @$pb.TagNumber(4)
  void clearLet() => clearField(4);
}

enum WriteModel_Ops {
  insertOneModel,
  deleteOneModel,
  replaceOneModel,
  updateOneModel,
  deleteManyModel,
  updateManyModel,
  notSet
}

class WriteModel extends $pb.GeneratedMessage {
  factory WriteModel({
    InsertOneModel? insertOneModel,
    DeleteOneModel? deleteOneModel,
    ReplaceOneModel? replaceOneModel,
    UpdateOneModel? updateOneModel,
    DeleteManyModel? deleteManyModel,
    UpdateManyModel? updateManyModel,
  }) {
    final $result = create();
    if (insertOneModel != null) {
      $result.insertOneModel = insertOneModel;
    }
    if (deleteOneModel != null) {
      $result.deleteOneModel = deleteOneModel;
    }
    if (replaceOneModel != null) {
      $result.replaceOneModel = replaceOneModel;
    }
    if (updateOneModel != null) {
      $result.updateOneModel = updateOneModel;
    }
    if (deleteManyModel != null) {
      $result.deleteManyModel = deleteManyModel;
    }
    if (updateManyModel != null) {
      $result.updateManyModel = updateManyModel;
    }
    return $result;
  }
  WriteModel._() : super();
  factory WriteModel.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory WriteModel.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static const $core.Map<$core.int, WriteModel_Ops> _WriteModel_OpsByTag = {
    1: WriteModel_Ops.insertOneModel,
    2: WriteModel_Ops.deleteOneModel,
    3: WriteModel_Ops.replaceOneModel,
    4: WriteModel_Ops.updateOneModel,
    5: WriteModel_Ops.deleteManyModel,
    6: WriteModel_Ops.updateManyModel,
    0: WriteModel_Ops.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'WriteModel',
      createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4, 5, 6])
    ..aOM<InsertOneModel>(1, _omitFieldNames ? '' : 'insertOneModel',
        protoName: 'insertOneModel', subBuilder: InsertOneModel.create)
    ..aOM<DeleteOneModel>(2, _omitFieldNames ? '' : 'deleteOneModel',
        protoName: 'deleteOneModel', subBuilder: DeleteOneModel.create)
    ..aOM<ReplaceOneModel>(3, _omitFieldNames ? '' : 'replaceOneModel',
        protoName: 'replaceOneModel', subBuilder: ReplaceOneModel.create)
    ..aOM<UpdateOneModel>(4, _omitFieldNames ? '' : 'updateOneModel',
        protoName: 'updateOneModel', subBuilder: UpdateOneModel.create)
    ..aOM<DeleteManyModel>(5, _omitFieldNames ? '' : 'deleteManyModel',
        protoName: 'deleteManyModel', subBuilder: DeleteManyModel.create)
    ..aOM<UpdateManyModel>(6, _omitFieldNames ? '' : 'updateManyModel',
        protoName: 'updateManyModel', subBuilder: UpdateManyModel.create)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  WriteModel clone() => WriteModel()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  WriteModel copyWith(void Function(WriteModel) updates) =>
      super.copyWith((message) => updates(message as WriteModel)) as WriteModel;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static WriteModel create() => WriteModel._();
  WriteModel createEmptyInstance() => create();
  static $pb.PbList<WriteModel> createRepeated() => $pb.PbList<WriteModel>();
  @$core.pragma('dart2js:noInline')
  static WriteModel getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<WriteModel>(create);
  static WriteModel? _defaultInstance;

  WriteModel_Ops whichOps() => _WriteModel_OpsByTag[$_whichOneof(0)]!;
  void clearOps() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  InsertOneModel get insertOneModel => $_getN(0);
  @$pb.TagNumber(1)
  set insertOneModel(InsertOneModel v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasInsertOneModel() => $_has(0);
  @$pb.TagNumber(1)
  void clearInsertOneModel() => clearField(1);
  @$pb.TagNumber(1)
  InsertOneModel ensureInsertOneModel() => $_ensure(0);

  @$pb.TagNumber(2)
  DeleteOneModel get deleteOneModel => $_getN(1);
  @$pb.TagNumber(2)
  set deleteOneModel(DeleteOneModel v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasDeleteOneModel() => $_has(1);
  @$pb.TagNumber(2)
  void clearDeleteOneModel() => clearField(2);
  @$pb.TagNumber(2)
  DeleteOneModel ensureDeleteOneModel() => $_ensure(1);

  @$pb.TagNumber(3)
  ReplaceOneModel get replaceOneModel => $_getN(2);
  @$pb.TagNumber(3)
  set replaceOneModel(ReplaceOneModel v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasReplaceOneModel() => $_has(2);
  @$pb.TagNumber(3)
  void clearReplaceOneModel() => clearField(3);
  @$pb.TagNumber(3)
  ReplaceOneModel ensureReplaceOneModel() => $_ensure(2);

  @$pb.TagNumber(4)
  UpdateOneModel get updateOneModel => $_getN(3);
  @$pb.TagNumber(4)
  set updateOneModel(UpdateOneModel v) {
    setField(4, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasUpdateOneModel() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdateOneModel() => clearField(4);
  @$pb.TagNumber(4)
  UpdateOneModel ensureUpdateOneModel() => $_ensure(3);

  @$pb.TagNumber(5)
  DeleteManyModel get deleteManyModel => $_getN(4);
  @$pb.TagNumber(5)
  set deleteManyModel(DeleteManyModel v) {
    setField(5, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasDeleteManyModel() => $_has(4);
  @$pb.TagNumber(5)
  void clearDeleteManyModel() => clearField(5);
  @$pb.TagNumber(5)
  DeleteManyModel ensureDeleteManyModel() => $_ensure(4);

  @$pb.TagNumber(6)
  UpdateManyModel get updateManyModel => $_getN(5);
  @$pb.TagNumber(6)
  set updateManyModel(UpdateManyModel v) {
    setField(6, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasUpdateManyModel() => $_has(5);
  @$pb.TagNumber(6)
  void clearUpdateManyModel() => clearField(6);
  @$pb.TagNumber(6)
  UpdateManyModel ensureUpdateManyModel() => $_ensure(5);
}

class InsertOneModel extends $pb.GeneratedMessage {
  factory InsertOneModel({
    $core.List<$core.int>? document,
  }) {
    final $result = create();
    if (document != null) {
      $result.document = document;
    }
    return $result;
  }
  InsertOneModel._() : super();
  factory InsertOneModel.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory InsertOneModel.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'InsertOneModel',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'document', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  InsertOneModel clone() => InsertOneModel()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  InsertOneModel copyWith(void Function(InsertOneModel) updates) =>
      super.copyWith((message) => updates(message as InsertOneModel))
          as InsertOneModel;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static InsertOneModel create() => InsertOneModel._();
  InsertOneModel createEmptyInstance() => create();
  static $pb.PbList<InsertOneModel> createRepeated() =>
      $pb.PbList<InsertOneModel>();
  @$core.pragma('dart2js:noInline')
  static InsertOneModel getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<InsertOneModel>(create);
  static InsertOneModel? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get document => $_getN(0);
  @$pb.TagNumber(1)
  set document($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasDocument() => $_has(0);
  @$pb.TagNumber(1)
  void clearDocument() => clearField(1);
}

class DeleteOneModel extends $pb.GeneratedMessage {
  factory DeleteOneModel({
    $core.List<$core.int>? filter,
    $core.List<$core.int>? hint,
  }) {
    final $result = create();
    if (filter != null) {
      $result.filter = filter;
    }
    if (hint != null) {
      $result.hint = hint;
    }
    return $result;
  }
  DeleteOneModel._() : super();
  factory DeleteOneModel.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DeleteOneModel.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'DeleteOneModel',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'filter', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'hint', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  DeleteOneModel clone() => DeleteOneModel()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  DeleteOneModel copyWith(void Function(DeleteOneModel) updates) =>
      super.copyWith((message) => updates(message as DeleteOneModel))
          as DeleteOneModel;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DeleteOneModel create() => DeleteOneModel._();
  DeleteOneModel createEmptyInstance() => create();
  static $pb.PbList<DeleteOneModel> createRepeated() =>
      $pb.PbList<DeleteOneModel>();
  @$core.pragma('dart2js:noInline')
  static DeleteOneModel getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DeleteOneModel>(create);
  static DeleteOneModel? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get filter => $_getN(0);
  @$pb.TagNumber(1)
  set filter($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasFilter() => $_has(0);
  @$pb.TagNumber(1)
  void clearFilter() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get hint => $_getN(1);
  @$pb.TagNumber(2)
  set hint($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasHint() => $_has(1);
  @$pb.TagNumber(2)
  void clearHint() => clearField(2);
}

class DeleteManyModel extends $pb.GeneratedMessage {
  factory DeleteManyModel({
    $core.List<$core.int>? filter,
    $core.List<$core.int>? hint,
  }) {
    final $result = create();
    if (filter != null) {
      $result.filter = filter;
    }
    if (hint != null) {
      $result.hint = hint;
    }
    return $result;
  }
  DeleteManyModel._() : super();
  factory DeleteManyModel.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DeleteManyModel.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'DeleteManyModel',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'filter', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'hint', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  DeleteManyModel clone() => DeleteManyModel()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  DeleteManyModel copyWith(void Function(DeleteManyModel) updates) =>
      super.copyWith((message) => updates(message as DeleteManyModel))
          as DeleteManyModel;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DeleteManyModel create() => DeleteManyModel._();
  DeleteManyModel createEmptyInstance() => create();
  static $pb.PbList<DeleteManyModel> createRepeated() =>
      $pb.PbList<DeleteManyModel>();
  @$core.pragma('dart2js:noInline')
  static DeleteManyModel getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DeleteManyModel>(create);
  static DeleteManyModel? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get filter => $_getN(0);
  @$pb.TagNumber(1)
  set filter($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasFilter() => $_has(0);
  @$pb.TagNumber(1)
  void clearFilter() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get hint => $_getN(1);
  @$pb.TagNumber(2)
  set hint($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasHint() => $_has(1);
  @$pb.TagNumber(2)
  void clearHint() => clearField(2);
}

class ReplaceOneModel extends $pb.GeneratedMessage {
  factory ReplaceOneModel({
    $3.BoolValue? upsert,
    $core.List<$core.int>? filter,
    $core.List<$core.int>? replacement,
    $core.List<$core.int>? hint,
  }) {
    final $result = create();
    if (upsert != null) {
      $result.upsert = upsert;
    }
    if (filter != null) {
      $result.filter = filter;
    }
    if (replacement != null) {
      $result.replacement = replacement;
    }
    if (hint != null) {
      $result.hint = hint;
    }
    return $result;
  }
  ReplaceOneModel._() : super();
  factory ReplaceOneModel.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory ReplaceOneModel.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'ReplaceOneModel',
      createEmptyInstance: create)
    ..aOM<$3.BoolValue>(1, _omitFieldNames ? '' : 'upsert',
        subBuilder: $3.BoolValue.create)
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'filter', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        3, _omitFieldNames ? '' : 'replacement', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        4, _omitFieldNames ? '' : 'hint', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  ReplaceOneModel clone() => ReplaceOneModel()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  ReplaceOneModel copyWith(void Function(ReplaceOneModel) updates) =>
      super.copyWith((message) => updates(message as ReplaceOneModel))
          as ReplaceOneModel;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static ReplaceOneModel create() => ReplaceOneModel._();
  ReplaceOneModel createEmptyInstance() => create();
  static $pb.PbList<ReplaceOneModel> createRepeated() =>
      $pb.PbList<ReplaceOneModel>();
  @$core.pragma('dart2js:noInline')
  static ReplaceOneModel getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<ReplaceOneModel>(create);
  static ReplaceOneModel? _defaultInstance;

  @$pb.TagNumber(1)
  $3.BoolValue get upsert => $_getN(0);
  @$pb.TagNumber(1)
  set upsert($3.BoolValue v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasUpsert() => $_has(0);
  @$pb.TagNumber(1)
  void clearUpsert() => clearField(1);
  @$pb.TagNumber(1)
  $3.BoolValue ensureUpsert() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.int> get filter => $_getN(1);
  @$pb.TagNumber(2)
  set filter($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasFilter() => $_has(1);
  @$pb.TagNumber(2)
  void clearFilter() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get replacement => $_getN(2);
  @$pb.TagNumber(3)
  set replacement($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasReplacement() => $_has(2);
  @$pb.TagNumber(3)
  void clearReplacement() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get hint => $_getN(3);
  @$pb.TagNumber(4)
  set hint($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasHint() => $_has(3);
  @$pb.TagNumber(4)
  void clearHint() => clearField(4);
}

class UpdateOneModel extends $pb.GeneratedMessage {
  factory UpdateOneModel({
    $3.BoolValue? upsert,
    $core.List<$core.int>? filter,
    $core.List<$core.int>? update,
    $core.List<$core.int>? hint,
  }) {
    final $result = create();
    if (upsert != null) {
      $result.upsert = upsert;
    }
    if (filter != null) {
      $result.filter = filter;
    }
    if (update != null) {
      $result.update = update;
    }
    if (hint != null) {
      $result.hint = hint;
    }
    return $result;
  }
  UpdateOneModel._() : super();
  factory UpdateOneModel.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory UpdateOneModel.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'UpdateOneModel',
      createEmptyInstance: create)
    ..aOM<$3.BoolValue>(1, _omitFieldNames ? '' : 'upsert',
        subBuilder: $3.BoolValue.create)
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'filter', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        3, _omitFieldNames ? '' : 'update', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        4, _omitFieldNames ? '' : 'hint', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  UpdateOneModel clone() => UpdateOneModel()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  UpdateOneModel copyWith(void Function(UpdateOneModel) updates) =>
      super.copyWith((message) => updates(message as UpdateOneModel))
          as UpdateOneModel;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static UpdateOneModel create() => UpdateOneModel._();
  UpdateOneModel createEmptyInstance() => create();
  static $pb.PbList<UpdateOneModel> createRepeated() =>
      $pb.PbList<UpdateOneModel>();
  @$core.pragma('dart2js:noInline')
  static UpdateOneModel getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<UpdateOneModel>(create);
  static UpdateOneModel? _defaultInstance;

  @$pb.TagNumber(1)
  $3.BoolValue get upsert => $_getN(0);
  @$pb.TagNumber(1)
  set upsert($3.BoolValue v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasUpsert() => $_has(0);
  @$pb.TagNumber(1)
  void clearUpsert() => clearField(1);
  @$pb.TagNumber(1)
  $3.BoolValue ensureUpsert() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.int> get filter => $_getN(1);
  @$pb.TagNumber(2)
  set filter($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasFilter() => $_has(1);
  @$pb.TagNumber(2)
  void clearFilter() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get update => $_getN(2);
  @$pb.TagNumber(3)
  set update($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasUpdate() => $_has(2);
  @$pb.TagNumber(3)
  void clearUpdate() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get hint => $_getN(3);
  @$pb.TagNumber(4)
  set hint($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasHint() => $_has(3);
  @$pb.TagNumber(4)
  void clearHint() => clearField(4);
}

class UpdateManyModel extends $pb.GeneratedMessage {
  factory UpdateManyModel({
    $3.BoolValue? upsert,
    $core.List<$core.int>? filter,
    $core.List<$core.int>? update,
    $core.List<$core.int>? hint,
  }) {
    final $result = create();
    if (upsert != null) {
      $result.upsert = upsert;
    }
    if (filter != null) {
      $result.filter = filter;
    }
    if (update != null) {
      $result.update = update;
    }
    if (hint != null) {
      $result.hint = hint;
    }
    return $result;
  }
  UpdateManyModel._() : super();
  factory UpdateManyModel.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory UpdateManyModel.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'UpdateManyModel',
      createEmptyInstance: create)
    ..aOM<$3.BoolValue>(1, _omitFieldNames ? '' : 'upsert',
        subBuilder: $3.BoolValue.create)
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'filter', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        3, _omitFieldNames ? '' : 'update', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        4, _omitFieldNames ? '' : 'hint', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  UpdateManyModel clone() => UpdateManyModel()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  UpdateManyModel copyWith(void Function(UpdateManyModel) updates) =>
      super.copyWith((message) => updates(message as UpdateManyModel))
          as UpdateManyModel;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static UpdateManyModel create() => UpdateManyModel._();
  UpdateManyModel createEmptyInstance() => create();
  static $pb.PbList<UpdateManyModel> createRepeated() =>
      $pb.PbList<UpdateManyModel>();
  @$core.pragma('dart2js:noInline')
  static UpdateManyModel getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<UpdateManyModel>(create);
  static UpdateManyModel? _defaultInstance;

  @$pb.TagNumber(1)
  $3.BoolValue get upsert => $_getN(0);
  @$pb.TagNumber(1)
  set upsert($3.BoolValue v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasUpsert() => $_has(0);
  @$pb.TagNumber(1)
  void clearUpsert() => clearField(1);
  @$pb.TagNumber(1)
  $3.BoolValue ensureUpsert() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.int> get filter => $_getN(1);
  @$pb.TagNumber(2)
  set filter($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasFilter() => $_has(1);
  @$pb.TagNumber(2)
  void clearFilter() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get update => $_getN(2);
  @$pb.TagNumber(3)
  set update($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasUpdate() => $_has(2);
  @$pb.TagNumber(3)
  void clearUpdate() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get hint => $_getN(3);
  @$pb.TagNumber(4)
  set hint($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasHint() => $_has(3);
  @$pb.TagNumber(4)
  void clearHint() => clearField(4);
}

class BulkWriteResult extends $pb.GeneratedMessage {
  factory BulkWriteResult({
    $fixnum.Int64? insertedCount,
    $fixnum.Int64? matchedCount,
    $fixnum.Int64? modifiedCount,
    $fixnum.Int64? deletedCount,
    $fixnum.Int64? upsertedCount,
    $core.Map<$fixnum.Int64, $core.List<$core.int>>? upsertedIds,
  }) {
    final $result = create();
    if (insertedCount != null) {
      $result.insertedCount = insertedCount;
    }
    if (matchedCount != null) {
      $result.matchedCount = matchedCount;
    }
    if (modifiedCount != null) {
      $result.modifiedCount = modifiedCount;
    }
    if (deletedCount != null) {
      $result.deletedCount = deletedCount;
    }
    if (upsertedCount != null) {
      $result.upsertedCount = upsertedCount;
    }
    if (upsertedIds != null) {
      $result.upsertedIds.addAll(upsertedIds);
    }
    return $result;
  }
  BulkWriteResult._() : super();
  factory BulkWriteResult.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory BulkWriteResult.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'BulkWriteResult',
      createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'insertedCount')
    ..aInt64(2, _omitFieldNames ? '' : 'matchedCount')
    ..aInt64(3, _omitFieldNames ? '' : 'modifiedCount')
    ..aInt64(4, _omitFieldNames ? '' : 'deletedCount')
    ..aInt64(5, _omitFieldNames ? '' : 'upsertedCount')
    ..m<$fixnum.Int64, $core.List<$core.int>>(
        6, _omitFieldNames ? '' : 'upsertedIds',
        entryClassName: 'BulkWriteResult.UpsertedIdsEntry',
        keyFieldType: $pb.PbFieldType.O6,
        valueFieldType: $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  BulkWriteResult clone() => BulkWriteResult()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  BulkWriteResult copyWith(void Function(BulkWriteResult) updates) =>
      super.copyWith((message) => updates(message as BulkWriteResult))
          as BulkWriteResult;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static BulkWriteResult create() => BulkWriteResult._();
  BulkWriteResult createEmptyInstance() => create();
  static $pb.PbList<BulkWriteResult> createRepeated() =>
      $pb.PbList<BulkWriteResult>();
  @$core.pragma('dart2js:noInline')
  static BulkWriteResult getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<BulkWriteResult>(create);
  static BulkWriteResult? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get insertedCount => $_getI64(0);
  @$pb.TagNumber(1)
  set insertedCount($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasInsertedCount() => $_has(0);
  @$pb.TagNumber(1)
  void clearInsertedCount() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get matchedCount => $_getI64(1);
  @$pb.TagNumber(2)
  set matchedCount($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasMatchedCount() => $_has(1);
  @$pb.TagNumber(2)
  void clearMatchedCount() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get modifiedCount => $_getI64(2);
  @$pb.TagNumber(3)
  set modifiedCount($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasModifiedCount() => $_has(2);
  @$pb.TagNumber(3)
  void clearModifiedCount() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get deletedCount => $_getI64(3);
  @$pb.TagNumber(4)
  set deletedCount($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasDeletedCount() => $_has(3);
  @$pb.TagNumber(4)
  void clearDeletedCount() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get upsertedCount => $_getI64(4);
  @$pb.TagNumber(5)
  set upsertedCount($fixnum.Int64 v) {
    $_setInt64(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasUpsertedCount() => $_has(4);
  @$pb.TagNumber(5)
  void clearUpsertedCount() => clearField(5);

  @$pb.TagNumber(6)
  $core.Map<$fixnum.Int64, $core.List<$core.int>> get upsertedIds =>
      $_getMap(5);
}

const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames =
    $core.bool.fromEnvironment('protobuf.omit_message_names');
