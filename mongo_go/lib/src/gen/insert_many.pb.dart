//
//  Generated code. Do not modify.
//  source: insert_many.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class InsertManyRequest extends $pb.GeneratedMessage {
  factory InsertManyRequest({
    $core.List<$core.int>? collectionOid,
    $core.List<$core.int>? sessionOid,
    $core.Iterable<$core.List<$core.int>>? documents,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    if (sessionOid != null) {
      $result.sessionOid = sessionOid;
    }
    if (documents != null) {
      $result.documents.addAll(documents);
    }
    return $result;
  }
  InsertManyRequest._() : super();
  factory InsertManyRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory InsertManyRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'InsertManyRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'sessionOid', $pb.PbFieldType.OY,
        protoName: 'sessionOid')
    ..p<$core.List<$core.int>>(
        3, _omitFieldNames ? '' : 'documents', $pb.PbFieldType.PY)
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  InsertManyRequest clone() => InsertManyRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  InsertManyRequest copyWith(void Function(InsertManyRequest) updates) =>
      super.copyWith((message) => updates(message as InsertManyRequest))
          as InsertManyRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static InsertManyRequest create() => InsertManyRequest._();
  InsertManyRequest createEmptyInstance() => create();
  static $pb.PbList<InsertManyRequest> createRepeated() =>
      $pb.PbList<InsertManyRequest>();
  @$core.pragma('dart2js:noInline')
  static InsertManyRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<InsertManyRequest>(create);
  static InsertManyRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get sessionOid => $_getN(1);
  @$pb.TagNumber(2)
  set sessionOid($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSessionOid() => $_has(1);
  @$pb.TagNumber(2)
  void clearSessionOid() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.List<$core.int>> get documents => $_getList(2);
}

class InsertManyResult extends $pb.GeneratedMessage {
  factory InsertManyResult({
    $core.Iterable<$core.List<$core.int>>? insertedIds,
  }) {
    final $result = create();
    if (insertedIds != null) {
      $result.insertedIds.addAll(insertedIds);
    }
    return $result;
  }
  InsertManyResult._() : super();
  factory InsertManyResult.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory InsertManyResult.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'InsertManyResult',
      createEmptyInstance: create)
    ..p<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'insertedIds', $pb.PbFieldType.PY,
        protoName: 'insertedIds')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  InsertManyResult clone() => InsertManyResult()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  InsertManyResult copyWith(void Function(InsertManyResult) updates) =>
      super.copyWith((message) => updates(message as InsertManyResult))
          as InsertManyResult;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static InsertManyResult create() => InsertManyResult._();
  InsertManyResult createEmptyInstance() => create();
  static $pb.PbList<InsertManyResult> createRepeated() =>
      $pb.PbList<InsertManyResult>();
  @$core.pragma('dart2js:noInline')
  static InsertManyResult getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<InsertManyResult>(create);
  static InsertManyResult? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.List<$core.int>> get insertedIds => $_getList(0);
}

const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames =
    $core.bool.fromEnvironment('protobuf.omit_message_names');
