//
//  Generated code. Do not modify.
//  source: errors.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use errorTypeDescriptor instead')
const ErrorType$json = {
  '1': 'ErrorType',
  '2': [
    {'1': 'unspecified', '2': 0},
    {'1': 'duplicate_key', '2': 1},
    {'1': 'network', '2': 2},
    {'1': 'timeout', '2': 3},
    {'1': 'no_documents', '2': 4},
  ],
};

/// Descriptor for `ErrorType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List errorTypeDescriptor = $convert.base64Decode(
    'CglFcnJvclR5cGUSDwoLdW5zcGVjaWZpZWQQABIRCg1kdXBsaWNhdGVfa2V5EAESCwoHbmV0d2'
    '9yaxACEgsKB3RpbWVvdXQQAxIQCgxub19kb2N1bWVudHMQBA==');

@$core.Deprecated('Use mongoErrorDescriptor instead')
const MongoError$json = {
  '1': 'MongoError',
  '2': [
    {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    {
      '1': 'errorType',
      '3': 2,
      '4': 1,
      '5': 14,
      '6': '.ErrorType',
      '10': 'errorType'
    },
  ],
};

/// Descriptor for `MongoError`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List mongoErrorDescriptor = $convert.base64Decode(
    'CgpNb25nb0Vycm9yEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USKAoJZXJyb3JUeXBlGAIgAS'
    'gOMgouRXJyb3JUeXBlUgllcnJvclR5cGU=');
