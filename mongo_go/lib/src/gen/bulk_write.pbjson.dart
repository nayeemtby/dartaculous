//
//  Generated code. Do not modify.
//  source: bulk_write.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use bulkWriteRequestDescriptor instead')
const BulkWriteRequest$json = {
  '1': 'BulkWriteRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
    {'1': 'sessionOid', '3': 2, '4': 1, '5': 12, '10': 'sessionOid'},
    {
      '1': 'writeModels',
      '3': 3,
      '4': 3,
      '5': 11,
      '6': '.WriteModel',
      '10': 'writeModels'
    },
    {
      '1': 'options',
      '3': 5,
      '4': 1,
      '5': 11,
      '6': '.BulkWriteOptions',
      '9': 0,
      '10': 'options'
    },
  ],
  '8': [
    {'1': 'opts'},
  ],
};

/// Descriptor for `BulkWriteRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bulkWriteRequestDescriptor = $convert.base64Decode(
    'ChBCdWxrV3JpdGVSZXF1ZXN0EiQKDWNvbGxlY3Rpb25PaWQYASABKAxSDWNvbGxlY3Rpb25PaW'
    'QSHgoKc2Vzc2lvbk9pZBgCIAEoDFIKc2Vzc2lvbk9pZBItCgt3cml0ZU1vZGVscxgDIAMoCzIL'
    'LldyaXRlTW9kZWxSC3dyaXRlTW9kZWxzEi0KB29wdGlvbnMYBSABKAsyES5CdWxrV3JpdGVPcH'
    'Rpb25zSABSB29wdGlvbnNCBgoEb3B0cw==');

@$core.Deprecated('Use bulkWriteOptionsDescriptor instead')
const BulkWriteOptions$json = {
  '1': 'BulkWriteOptions',
  '2': [
    {
      '1': 'bypass_document_validation',
      '3': 1,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'bypassDocumentValidation'
    },
    {'1': 'comment', '3': 2, '4': 1, '5': 12, '10': 'comment'},
    {
      '1': 'ordered',
      '3': 3,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'ordered'
    },
    {'1': 'let', '3': 4, '4': 1, '5': 12, '10': 'let'},
  ],
};

/// Descriptor for `BulkWriteOptions`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bulkWriteOptionsDescriptor = $convert.base64Decode(
    'ChBCdWxrV3JpdGVPcHRpb25zElgKGmJ5cGFzc19kb2N1bWVudF92YWxpZGF0aW9uGAEgASgLMh'
    'ouZ29vZ2xlLnByb3RvYnVmLkJvb2xWYWx1ZVIYYnlwYXNzRG9jdW1lbnRWYWxpZGF0aW9uEhgK'
    'B2NvbW1lbnQYAiABKAxSB2NvbW1lbnQSNAoHb3JkZXJlZBgDIAEoCzIaLmdvb2dsZS5wcm90b2'
    'J1Zi5Cb29sVmFsdWVSB29yZGVyZWQSEAoDbGV0GAQgASgMUgNsZXQ=');

@$core.Deprecated('Use writeModelDescriptor instead')
const WriteModel$json = {
  '1': 'WriteModel',
  '2': [
    {
      '1': 'insertOneModel',
      '3': 1,
      '4': 1,
      '5': 11,
      '6': '.InsertOneModel',
      '9': 0,
      '10': 'insertOneModel'
    },
    {
      '1': 'deleteOneModel',
      '3': 2,
      '4': 1,
      '5': 11,
      '6': '.DeleteOneModel',
      '9': 0,
      '10': 'deleteOneModel'
    },
    {
      '1': 'replaceOneModel',
      '3': 3,
      '4': 1,
      '5': 11,
      '6': '.ReplaceOneModel',
      '9': 0,
      '10': 'replaceOneModel'
    },
    {
      '1': 'updateOneModel',
      '3': 4,
      '4': 1,
      '5': 11,
      '6': '.UpdateOneModel',
      '9': 0,
      '10': 'updateOneModel'
    },
    {
      '1': 'deleteManyModel',
      '3': 5,
      '4': 1,
      '5': 11,
      '6': '.DeleteManyModel',
      '9': 0,
      '10': 'deleteManyModel'
    },
    {
      '1': 'updateManyModel',
      '3': 6,
      '4': 1,
      '5': 11,
      '6': '.UpdateManyModel',
      '9': 0,
      '10': 'updateManyModel'
    },
  ],
  '8': [
    {'1': 'ops'},
  ],
};

/// Descriptor for `WriteModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List writeModelDescriptor = $convert.base64Decode(
    'CgpXcml0ZU1vZGVsEjkKDmluc2VydE9uZU1vZGVsGAEgASgLMg8uSW5zZXJ0T25lTW9kZWxIAF'
    'IOaW5zZXJ0T25lTW9kZWwSOQoOZGVsZXRlT25lTW9kZWwYAiABKAsyDy5EZWxldGVPbmVNb2Rl'
    'bEgAUg5kZWxldGVPbmVNb2RlbBI8Cg9yZXBsYWNlT25lTW9kZWwYAyABKAsyEC5SZXBsYWNlT2'
    '5lTW9kZWxIAFIPcmVwbGFjZU9uZU1vZGVsEjkKDnVwZGF0ZU9uZU1vZGVsGAQgASgLMg8uVXBk'
    'YXRlT25lTW9kZWxIAFIOdXBkYXRlT25lTW9kZWwSPAoPZGVsZXRlTWFueU1vZGVsGAUgASgLMh'
    'AuRGVsZXRlTWFueU1vZGVsSABSD2RlbGV0ZU1hbnlNb2RlbBI8Cg91cGRhdGVNYW55TW9kZWwY'
    'BiABKAsyEC5VcGRhdGVNYW55TW9kZWxIAFIPdXBkYXRlTWFueU1vZGVsQgUKA29wcw==');

@$core.Deprecated('Use insertOneModelDescriptor instead')
const InsertOneModel$json = {
  '1': 'InsertOneModel',
  '2': [
    {'1': 'document', '3': 1, '4': 1, '5': 12, '10': 'document'},
  ],
};

/// Descriptor for `InsertOneModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List insertOneModelDescriptor = $convert.base64Decode(
    'Cg5JbnNlcnRPbmVNb2RlbBIaCghkb2N1bWVudBgBIAEoDFIIZG9jdW1lbnQ=');

@$core.Deprecated('Use deleteOneModelDescriptor instead')
const DeleteOneModel$json = {
  '1': 'DeleteOneModel',
  '2': [
    {'1': 'filter', '3': 1, '4': 1, '5': 12, '10': 'filter'},
    {'1': 'hint', '3': 2, '4': 1, '5': 12, '10': 'hint'},
  ],
};

/// Descriptor for `DeleteOneModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteOneModelDescriptor = $convert.base64Decode(
    'Cg5EZWxldGVPbmVNb2RlbBIWCgZmaWx0ZXIYASABKAxSBmZpbHRlchISCgRoaW50GAIgASgMUg'
    'RoaW50');

@$core.Deprecated('Use deleteManyModelDescriptor instead')
const DeleteManyModel$json = {
  '1': 'DeleteManyModel',
  '2': [
    {'1': 'filter', '3': 1, '4': 1, '5': 12, '10': 'filter'},
    {'1': 'hint', '3': 2, '4': 1, '5': 12, '10': 'hint'},
  ],
};

/// Descriptor for `DeleteManyModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteManyModelDescriptor = $convert.base64Decode(
    'Cg9EZWxldGVNYW55TW9kZWwSFgoGZmlsdGVyGAEgASgMUgZmaWx0ZXISEgoEaGludBgCIAEoDF'
    'IEaGludA==');

@$core.Deprecated('Use replaceOneModelDescriptor instead')
const ReplaceOneModel$json = {
  '1': 'ReplaceOneModel',
  '2': [
    {
      '1': 'upsert',
      '3': 1,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'upsert'
    },
    {'1': 'filter', '3': 2, '4': 1, '5': 12, '10': 'filter'},
    {'1': 'replacement', '3': 3, '4': 1, '5': 12, '10': 'replacement'},
    {'1': 'hint', '3': 4, '4': 1, '5': 12, '10': 'hint'},
  ],
};

/// Descriptor for `ReplaceOneModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List replaceOneModelDescriptor = $convert.base64Decode(
    'Cg9SZXBsYWNlT25lTW9kZWwSMgoGdXBzZXJ0GAEgASgLMhouZ29vZ2xlLnByb3RvYnVmLkJvb2'
    'xWYWx1ZVIGdXBzZXJ0EhYKBmZpbHRlchgCIAEoDFIGZmlsdGVyEiAKC3JlcGxhY2VtZW50GAMg'
    'ASgMUgtyZXBsYWNlbWVudBISCgRoaW50GAQgASgMUgRoaW50');

@$core.Deprecated('Use updateOneModelDescriptor instead')
const UpdateOneModel$json = {
  '1': 'UpdateOneModel',
  '2': [
    {
      '1': 'upsert',
      '3': 1,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'upsert'
    },
    {'1': 'filter', '3': 2, '4': 1, '5': 12, '10': 'filter'},
    {'1': 'update', '3': 3, '4': 1, '5': 12, '10': 'update'},
    {'1': 'hint', '3': 4, '4': 1, '5': 12, '10': 'hint'},
  ],
};

/// Descriptor for `UpdateOneModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateOneModelDescriptor = $convert.base64Decode(
    'Cg5VcGRhdGVPbmVNb2RlbBIyCgZ1cHNlcnQYASABKAsyGi5nb29nbGUucHJvdG9idWYuQm9vbF'
    'ZhbHVlUgZ1cHNlcnQSFgoGZmlsdGVyGAIgASgMUgZmaWx0ZXISFgoGdXBkYXRlGAMgASgMUgZ1'
    'cGRhdGUSEgoEaGludBgEIAEoDFIEaGludA==');

@$core.Deprecated('Use updateManyModelDescriptor instead')
const UpdateManyModel$json = {
  '1': 'UpdateManyModel',
  '2': [
    {
      '1': 'upsert',
      '3': 1,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'upsert'
    },
    {'1': 'filter', '3': 2, '4': 1, '5': 12, '10': 'filter'},
    {'1': 'update', '3': 3, '4': 1, '5': 12, '10': 'update'},
    {'1': 'hint', '3': 4, '4': 1, '5': 12, '10': 'hint'},
  ],
};

/// Descriptor for `UpdateManyModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateManyModelDescriptor = $convert.base64Decode(
    'Cg9VcGRhdGVNYW55TW9kZWwSMgoGdXBzZXJ0GAEgASgLMhouZ29vZ2xlLnByb3RvYnVmLkJvb2'
    'xWYWx1ZVIGdXBzZXJ0EhYKBmZpbHRlchgCIAEoDFIGZmlsdGVyEhYKBnVwZGF0ZRgDIAEoDFIG'
    'dXBkYXRlEhIKBGhpbnQYBCABKAxSBGhpbnQ=');

@$core.Deprecated('Use bulkWriteResultDescriptor instead')
const BulkWriteResult$json = {
  '1': 'BulkWriteResult',
  '2': [
    {'1': 'inserted_count', '3': 1, '4': 1, '5': 3, '10': 'insertedCount'},
    {'1': 'matched_count', '3': 2, '4': 1, '5': 3, '10': 'matchedCount'},
    {'1': 'modified_count', '3': 3, '4': 1, '5': 3, '10': 'modifiedCount'},
    {'1': 'deleted_count', '3': 4, '4': 1, '5': 3, '10': 'deletedCount'},
    {'1': 'upserted_count', '3': 5, '4': 1, '5': 3, '10': 'upsertedCount'},
    {
      '1': 'upserted_ids',
      '3': 6,
      '4': 3,
      '5': 11,
      '6': '.BulkWriteResult.UpsertedIdsEntry',
      '10': 'upsertedIds'
    },
  ],
  '3': [BulkWriteResult_UpsertedIdsEntry$json],
};

@$core.Deprecated('Use bulkWriteResultDescriptor instead')
const BulkWriteResult_UpsertedIdsEntry$json = {
  '1': 'UpsertedIdsEntry',
  '2': [
    {'1': 'key', '3': 1, '4': 1, '5': 3, '10': 'key'},
    {'1': 'value', '3': 2, '4': 1, '5': 12, '10': 'value'},
  ],
  '7': {'7': true},
};

/// Descriptor for `BulkWriteResult`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bulkWriteResultDescriptor = $convert.base64Decode(
    'Cg9CdWxrV3JpdGVSZXN1bHQSJQoOaW5zZXJ0ZWRfY291bnQYASABKANSDWluc2VydGVkQ291bn'
    'QSIwoNbWF0Y2hlZF9jb3VudBgCIAEoA1IMbWF0Y2hlZENvdW50EiUKDm1vZGlmaWVkX2NvdW50'
    'GAMgASgDUg1tb2RpZmllZENvdW50EiMKDWRlbGV0ZWRfY291bnQYBCABKANSDGRlbGV0ZWRDb3'
    'VudBIlCg51cHNlcnRlZF9jb3VudBgFIAEoA1INdXBzZXJ0ZWRDb3VudBJECgx1cHNlcnRlZF9p'
    'ZHMYBiADKAsyIS5CdWxrV3JpdGVSZXN1bHQuVXBzZXJ0ZWRJZHNFbnRyeVILdXBzZXJ0ZWRJZH'
    'MaPgoQVXBzZXJ0ZWRJZHNFbnRyeRIQCgNrZXkYASABKANSA2tleRIUCgV2YWx1ZRgCIAEoDFIF'
    'dmFsdWU6AjgB');
