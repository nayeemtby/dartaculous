//
//  Generated code. Do not modify.
//  source: list_database_names_response.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use listDatabaseNamesResponseDescriptor instead')
const ListDatabaseNamesResponse$json = {
  '1': 'ListDatabaseNamesResponse',
  '2': [
    {'1': 'names', '3': 1, '4': 3, '5': 9, '10': 'names'},
  ],
};

/// Descriptor for `ListDatabaseNamesResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listDatabaseNamesResponseDescriptor =
    $convert.base64Decode(
        'ChlMaXN0RGF0YWJhc2VOYW1lc1Jlc3BvbnNlEhQKBW5hbWVzGAEgAygJUgVuYW1lcw==');
