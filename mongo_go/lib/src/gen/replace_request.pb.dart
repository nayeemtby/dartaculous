//
//  Generated code. Do not modify.
//  source: replace_request.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class ReplaceRequest extends $pb.GeneratedMessage {
  factory ReplaceRequest({
    $core.List<$core.int>? collectionOid,
    $core.List<$core.int>? sessionOid,
    $core.List<$core.int>? filter,
    $core.List<$core.int>? replacement,
    $core.bool? isUpsert,
  }) {
    final $result = create();
    if (collectionOid != null) {
      $result.collectionOid = collectionOid;
    }
    if (sessionOid != null) {
      $result.sessionOid = sessionOid;
    }
    if (filter != null) {
      $result.filter = filter;
    }
    if (replacement != null) {
      $result.replacement = replacement;
    }
    if (isUpsert != null) {
      $result.isUpsert = isUpsert;
    }
    return $result;
  }
  ReplaceRequest._() : super();
  factory ReplaceRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory ReplaceRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      _omitMessageNames ? '' : 'ReplaceRequest',
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(
        1, _omitFieldNames ? '' : 'collectionOid', $pb.PbFieldType.OY,
        protoName: 'collectionOid')
    ..a<$core.List<$core.int>>(
        2, _omitFieldNames ? '' : 'sessionOid', $pb.PbFieldType.OY,
        protoName: 'sessionOid')
    ..a<$core.List<$core.int>>(
        3, _omitFieldNames ? '' : 'filter', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(
        4, _omitFieldNames ? '' : 'replacement', $pb.PbFieldType.OY)
    ..aOB(5, _omitFieldNames ? '' : 'isUpsert', protoName: 'isUpsert')
    ..hasRequiredFields = false;

  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
      'Will be removed in next major version')
  ReplaceRequest clone() => ReplaceRequest()..mergeFromMessage(this);
  @$core.Deprecated('Using this can add significant overhead to your binary. '
      'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
      'Will be removed in next major version')
  ReplaceRequest copyWith(void Function(ReplaceRequest) updates) =>
      super.copyWith((message) => updates(message as ReplaceRequest))
          as ReplaceRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static ReplaceRequest create() => ReplaceRequest._();
  ReplaceRequest createEmptyInstance() => create();
  static $pb.PbList<ReplaceRequest> createRepeated() =>
      $pb.PbList<ReplaceRequest>();
  @$core.pragma('dart2js:noInline')
  static ReplaceRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<ReplaceRequest>(create);
  static ReplaceRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get collectionOid => $_getN(0);
  @$pb.TagNumber(1)
  set collectionOid($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCollectionOid() => $_has(0);
  @$pb.TagNumber(1)
  void clearCollectionOid() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get sessionOid => $_getN(1);
  @$pb.TagNumber(2)
  set sessionOid($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSessionOid() => $_has(1);
  @$pb.TagNumber(2)
  void clearSessionOid() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get filter => $_getN(2);
  @$pb.TagNumber(3)
  set filter($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasFilter() => $_has(2);
  @$pb.TagNumber(3)
  void clearFilter() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get replacement => $_getN(3);
  @$pb.TagNumber(4)
  set replacement($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasReplacement() => $_has(3);
  @$pb.TagNumber(4)
  void clearReplacement() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get isUpsert => $_getBF(4);
  @$pb.TagNumber(5)
  set isUpsert($core.bool v) {
    $_setBool(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasIsUpsert() => $_has(4);
  @$pb.TagNumber(5)
  void clearIsUpsert() => clearField(5);
}

const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames =
    $core.bool.fromEnvironment('protobuf.omit_message_names');
