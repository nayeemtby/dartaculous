//
//  Generated code. Do not modify.
//  source: index_requests.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use collationDescriptor instead')
const Collation$json = {
  '1': 'Collation',
  '2': [
    {'1': 'Locale', '3': 1, '4': 1, '5': 9, '10': 'Locale'},
    {'1': 'CaseLevel', '3': 2, '4': 1, '5': 8, '10': 'CaseLevel'},
    {'1': 'CaseFirst', '3': 3, '4': 1, '5': 9, '10': 'CaseFirst'},
    {'1': 'Strength', '3': 4, '4': 1, '5': 5, '10': 'Strength'},
    {'1': 'NumericOrdering', '3': 5, '4': 1, '5': 8, '10': 'NumericOrdering'},
    {'1': 'Alternate', '3': 6, '4': 1, '5': 9, '10': 'Alternate'},
    {'1': 'MaxVariable', '3': 7, '4': 1, '5': 9, '10': 'MaxVariable'},
    {'1': 'Normalization', '3': 8, '4': 1, '5': 8, '10': 'Normalization'},
    {'1': 'Backwards', '3': 9, '4': 1, '5': 8, '10': 'Backwards'},
  ],
};

/// Descriptor for `Collation`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List collationDescriptor = $convert.base64Decode(
    'CglDb2xsYXRpb24SFgoGTG9jYWxlGAEgASgJUgZMb2NhbGUSHAoJQ2FzZUxldmVsGAIgASgIUg'
    'lDYXNlTGV2ZWwSHAoJQ2FzZUZpcnN0GAMgASgJUglDYXNlRmlyc3QSGgoIU3RyZW5ndGgYBCAB'
    'KAVSCFN0cmVuZ3RoEigKD051bWVyaWNPcmRlcmluZxgFIAEoCFIPTnVtZXJpY09yZGVyaW5nEh'
    'wKCUFsdGVybmF0ZRgGIAEoCVIJQWx0ZXJuYXRlEiAKC01heFZhcmlhYmxlGAcgASgJUgtNYXhW'
    'YXJpYWJsZRIkCg1Ob3JtYWxpemF0aW9uGAggASgIUg1Ob3JtYWxpemF0aW9uEhwKCUJhY2t3YX'
    'JkcxgJIAEoCFIJQmFja3dhcmRz');

@$core.Deprecated('Use indexOptionsDescriptor instead')
const IndexOptions$json = {
  '1': 'IndexOptions',
  '2': [
    {
      '1': 'background',
      '3': 1,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'background'
    },
    {
      '1': 'expireAfterSeconds',
      '3': 2,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.Int32Value',
      '10': 'expireAfterSeconds'
    },
    {
      '1': 'name',
      '3': 3,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.StringValue',
      '10': 'name'
    },
    {
      '1': 'sparse',
      '3': 4,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'sparse'
    },
    {'1': 'storageEngine', '3': 5, '4': 1, '5': 12, '10': 'storageEngine'},
    {
      '1': 'unique',
      '3': 6,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'unique'
    },
    {
      '1': 'version',
      '3': 7,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.Int32Value',
      '10': 'version'
    },
    {
      '1': 'defaultLanguage',
      '3': 8,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.StringValue',
      '10': 'defaultLanguage'
    },
    {
      '1': 'languageOverride',
      '3': 9,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.StringValue',
      '10': 'languageOverride'
    },
    {
      '1': 'textVersion',
      '3': 10,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.Int32Value',
      '10': 'textVersion'
    },
    {'1': 'weights', '3': 11, '4': 1, '5': 12, '10': 'weights'},
    {
      '1': 'sphereVersion',
      '3': 12,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.Int32Value',
      '10': 'sphereVersion'
    },
    {
      '1': 'bits',
      '3': 13,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.Int32Value',
      '10': 'bits'
    },
    {
      '1': 'max',
      '3': 14,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.DoubleValue',
      '10': 'max'
    },
    {
      '1': 'min',
      '3': 15,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.DoubleValue',
      '10': 'min'
    },
    {
      '1': 'bucketSize',
      '3': 16,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.Int32Value',
      '10': 'bucketSize'
    },
    {
      '1': 'partialFilterExpression',
      '3': 17,
      '4': 1,
      '5': 12,
      '10': 'partialFilterExpression'
    },
    {'1': 'collation', '3': 18, '4': 1, '5': 12, '10': 'collation'},
    {
      '1': 'wildcardProjection',
      '3': 19,
      '4': 1,
      '5': 12,
      '10': 'wildcardProjection'
    },
    {
      '1': 'hidden',
      '3': 20,
      '4': 1,
      '5': 11,
      '6': '.google.protobuf.BoolValue',
      '10': 'hidden'
    },
  ],
};

/// Descriptor for `IndexOptions`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List indexOptionsDescriptor = $convert.base64Decode(
    'CgxJbmRleE9wdGlvbnMSOgoKYmFja2dyb3VuZBgBIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5Cb2'
    '9sVmFsdWVSCmJhY2tncm91bmQSSwoSZXhwaXJlQWZ0ZXJTZWNvbmRzGAIgASgLMhsuZ29vZ2xl'
    'LnByb3RvYnVmLkludDMyVmFsdWVSEmV4cGlyZUFmdGVyU2Vjb25kcxIwCgRuYW1lGAMgASgLMh'
    'wuZ29vZ2xlLnByb3RvYnVmLlN0cmluZ1ZhbHVlUgRuYW1lEjIKBnNwYXJzZRgEIAEoCzIaLmdv'
    'b2dsZS5wcm90b2J1Zi5Cb29sVmFsdWVSBnNwYXJzZRIkCg1zdG9yYWdlRW5naW5lGAUgASgMUg'
    '1zdG9yYWdlRW5naW5lEjIKBnVuaXF1ZRgGIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5Cb29sVmFs'
    'dWVSBnVuaXF1ZRI1Cgd2ZXJzaW9uGAcgASgLMhsuZ29vZ2xlLnByb3RvYnVmLkludDMyVmFsdW'
    'VSB3ZlcnNpb24SRgoPZGVmYXVsdExhbmd1YWdlGAggASgLMhwuZ29vZ2xlLnByb3RvYnVmLlN0'
    'cmluZ1ZhbHVlUg9kZWZhdWx0TGFuZ3VhZ2USSAoQbGFuZ3VhZ2VPdmVycmlkZRgJIAEoCzIcLm'
    'dvb2dsZS5wcm90b2J1Zi5TdHJpbmdWYWx1ZVIQbGFuZ3VhZ2VPdmVycmlkZRI9Cgt0ZXh0VmVy'
    'c2lvbhgKIAEoCzIbLmdvb2dsZS5wcm90b2J1Zi5JbnQzMlZhbHVlUgt0ZXh0VmVyc2lvbhIYCg'
    'd3ZWlnaHRzGAsgASgMUgd3ZWlnaHRzEkEKDXNwaGVyZVZlcnNpb24YDCABKAsyGy5nb29nbGUu'
    'cHJvdG9idWYuSW50MzJWYWx1ZVINc3BoZXJlVmVyc2lvbhIvCgRiaXRzGA0gASgLMhsuZ29vZ2'
    'xlLnByb3RvYnVmLkludDMyVmFsdWVSBGJpdHMSLgoDbWF4GA4gASgLMhwuZ29vZ2xlLnByb3Rv'
    'YnVmLkRvdWJsZVZhbHVlUgNtYXgSLgoDbWluGA8gASgLMhwuZ29vZ2xlLnByb3RvYnVmLkRvdW'
    'JsZVZhbHVlUgNtaW4SOwoKYnVja2V0U2l6ZRgQIAEoCzIbLmdvb2dsZS5wcm90b2J1Zi5JbnQz'
    'MlZhbHVlUgpidWNrZXRTaXplEjgKF3BhcnRpYWxGaWx0ZXJFeHByZXNzaW9uGBEgASgMUhdwYX'
    'J0aWFsRmlsdGVyRXhwcmVzc2lvbhIcCgljb2xsYXRpb24YEiABKAxSCWNvbGxhdGlvbhIuChJ3'
    'aWxkY2FyZFByb2plY3Rpb24YEyABKAxSEndpbGRjYXJkUHJvamVjdGlvbhIyCgZoaWRkZW4YFC'
    'ABKAsyGi5nb29nbGUucHJvdG9idWYuQm9vbFZhbHVlUgZoaWRkZW4=');

@$core.Deprecated('Use createIndexRequestDescriptor instead')
const CreateIndexRequest$json = {
  '1': 'CreateIndexRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
    {'1': 'keys', '3': 2, '4': 1, '5': 12, '10': 'keys'},
    {'1': 'indexOptions', '3': 3, '4': 1, '5': 12, '10': 'indexOptions'},
  ],
};

/// Descriptor for `CreateIndexRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createIndexRequestDescriptor = $convert.base64Decode(
    'ChJDcmVhdGVJbmRleFJlcXVlc3QSJAoNY29sbGVjdGlvbk9pZBgBIAEoDFINY29sbGVjdGlvbk'
    '9pZBISCgRrZXlzGAIgASgMUgRrZXlzEiIKDGluZGV4T3B0aW9ucxgDIAEoDFIMaW5kZXhPcHRp'
    'b25z');

@$core.Deprecated('Use dropIndexRequestDescriptor instead')
const DropIndexRequest$json = {
  '1': 'DropIndexRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
    {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `DropIndexRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dropIndexRequestDescriptor = $convert.base64Decode(
    'ChBEcm9wSW5kZXhSZXF1ZXN0EiQKDWNvbGxlY3Rpb25PaWQYASABKAxSDWNvbGxlY3Rpb25PaW'
    'QSEgoEbmFtZRgCIAEoCVIEbmFtZQ==');

@$core.Deprecated('Use dropAllIndexesRequestDescriptor instead')
const DropAllIndexesRequest$json = {
  '1': 'DropAllIndexesRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
  ],
};

/// Descriptor for `DropAllIndexesRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dropAllIndexesRequestDescriptor = $convert.base64Decode(
    'ChVEcm9wQWxsSW5kZXhlc1JlcXVlc3QSJAoNY29sbGVjdGlvbk9pZBgBIAEoDFINY29sbGVjdG'
    'lvbk9pZA==');

@$core.Deprecated('Use listIndexesRequestDescriptor instead')
const ListIndexesRequest$json = {
  '1': 'ListIndexesRequest',
  '2': [
    {'1': 'collectionOid', '3': 1, '4': 1, '5': 12, '10': 'collectionOid'},
  ],
};

/// Descriptor for `ListIndexesRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listIndexesRequestDescriptor = $convert.base64Decode(
    'ChJMaXN0SW5kZXhlc1JlcXVlc3QSJAoNY29sbGVjdGlvbk9pZBgBIAEoDFINY29sbGVjdGlvbk'
    '9pZA==');
