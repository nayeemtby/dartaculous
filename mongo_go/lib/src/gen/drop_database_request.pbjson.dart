//
//  Generated code. Do not modify.
//  source: drop_database_request.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use dropDatabaseRequestDescriptor instead')
const DropDatabaseRequest$json = {
  '1': 'DropDatabaseRequest',
  '2': [
    {'1': 'databaseOid', '3': 1, '4': 1, '5': 12, '10': 'databaseOid'},
  ],
};

/// Descriptor for `DropDatabaseRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dropDatabaseRequestDescriptor = $convert.base64Decode(
    'ChNEcm9wRGF0YWJhc2VSZXF1ZXN0EiAKC2RhdGFiYXNlT2lkGAEgASgMUgtkYXRhYmFzZU9pZA'
    '==');
