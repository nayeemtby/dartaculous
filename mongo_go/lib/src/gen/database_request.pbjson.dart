//
//  Generated code. Do not modify.
//  source: database_request.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use databaseRequestDescriptor instead')
const DatabaseRequest$json = {
  '1': 'DatabaseRequest',
  '2': [
    {'1': 'connectionOid', '3': 1, '4': 1, '5': 12, '10': 'connectionOid'},
    {'1': 'databaseName', '3': 2, '4': 1, '5': 9, '10': 'databaseName'},
  ],
};

/// Descriptor for `DatabaseRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List databaseRequestDescriptor = $convert.base64Decode(
    'Cg9EYXRhYmFzZVJlcXVlc3QSJAoNY29ubmVjdGlvbk9pZBgBIAEoDFINY29ubmVjdGlvbk9pZB'
    'IiCgxkYXRhYmFzZU5hbWUYAiABKAlSDGRhdGFiYXNlTmFtZQ==');
