/// When applied as an annotation to a field, indicates the code
/// generator to map that field.
///
/// This annotation can be particularly useful to
/// map fields that should have a different name when
/// mapped to the Map<String, dynamic>.
class MapField {
  final String? name;
  final bool? isKey;

  /// Expression to be added as a null fallback for when the map doesn't
  /// include the value for a new non-nullable property.
  final String? nullFallback;

  const MapField({
    this.name,
    this.isKey,
    this.nullFallback,
  });
}
