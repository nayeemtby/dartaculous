import 'package:proto_annotations/proto_annotations.dart';
import 'package:proto_generator_test/grpc/google/protobuf/wrappers.pb.dart';
import 'package:proto_generator_test/grpc/model.pb.dart';

part 'encoding_target.g.dart';

@proto
class EncodingTarget {
  @ProtoField(1)
  final int? someValue;

  EncodingTarget({this.someValue});
}
